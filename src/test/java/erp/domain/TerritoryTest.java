package erp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import erp.web.rest.TestUtil;

public class TerritoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Territory.class);
        Territory territory1 = new Territory();
        territory1.setId(1L);
        Territory territory2 = new Territory();
        territory2.setId(territory1.getId());
        assertThat(territory1).isEqualTo(territory2);
        territory2.setId(2L);
        assertThat(territory1).isNotEqualTo(territory2);
        territory1.setId(null);
        assertThat(territory1).isNotEqualTo(territory2);
    }
}
