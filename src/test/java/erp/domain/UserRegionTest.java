package erp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import erp.web.rest.TestUtil;

public class UserRegionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserRegion.class);
        UserRegion userRegion1 = new UserRegion();
        userRegion1.setId(1L);
        UserRegion userRegion2 = new UserRegion();
        userRegion2.setId(userRegion1.getId());
        assertThat(userRegion1).isEqualTo(userRegion2);
        userRegion2.setId(2L);
        assertThat(userRegion1).isNotEqualTo(userRegion2);
        userRegion1.setId(null);
        assertThat(userRegion1).isNotEqualTo(userRegion2);
    }
}
