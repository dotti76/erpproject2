package erp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import erp.web.rest.TestUtil;

public class UserTerritoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserTerritory.class);
        UserTerritory userTerritory1 = new UserTerritory();
        userTerritory1.setId(1L);
        UserTerritory userTerritory2 = new UserTerritory();
        userTerritory2.setId(userTerritory1.getId());
        assertThat(userTerritory1).isEqualTo(userTerritory2);
        userTerritory2.setId(2L);
        assertThat(userTerritory1).isNotEqualTo(userTerritory2);
        userTerritory1.setId(null);
        assertThat(userTerritory1).isNotEqualTo(userTerritory2);
    }
}
