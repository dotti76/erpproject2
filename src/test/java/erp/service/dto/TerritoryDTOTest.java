package erp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import erp.web.rest.TestUtil;

public class TerritoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TerritoryDTO.class);
        TerritoryDTO territoryDTO1 = new TerritoryDTO();
        territoryDTO1.setId(1L);
        TerritoryDTO territoryDTO2 = new TerritoryDTO();
        assertThat(territoryDTO1).isNotEqualTo(territoryDTO2);
        territoryDTO2.setId(territoryDTO1.getId());
        assertThat(territoryDTO1).isEqualTo(territoryDTO2);
        territoryDTO2.setId(2L);
        assertThat(territoryDTO1).isNotEqualTo(territoryDTO2);
        territoryDTO1.setId(null);
        assertThat(territoryDTO1).isNotEqualTo(territoryDTO2);
    }
}
