package erp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import erp.web.rest.TestUtil;

public class UserTerritoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserTerritoryDTO.class);
        UserTerritoryDTO userTerritoryDTO1 = new UserTerritoryDTO();
        userTerritoryDTO1.setId(1L);
        UserTerritoryDTO userTerritoryDTO2 = new UserTerritoryDTO();
        assertThat(userTerritoryDTO1).isNotEqualTo(userTerritoryDTO2);
        userTerritoryDTO2.setId(userTerritoryDTO1.getId());
        assertThat(userTerritoryDTO1).isEqualTo(userTerritoryDTO2);
        userTerritoryDTO2.setId(2L);
        assertThat(userTerritoryDTO1).isNotEqualTo(userTerritoryDTO2);
        userTerritoryDTO1.setId(null);
        assertThat(userTerritoryDTO1).isNotEqualTo(userTerritoryDTO2);
    }
}
