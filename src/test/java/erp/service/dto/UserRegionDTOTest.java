package erp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import erp.web.rest.TestUtil;

public class UserRegionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserRegionDTO.class);
        UserRegionDTO userRegionDTO1 = new UserRegionDTO();
        userRegionDTO1.setId(1L);
        UserRegionDTO userRegionDTO2 = new UserRegionDTO();
        assertThat(userRegionDTO1).isNotEqualTo(userRegionDTO2);
        userRegionDTO2.setId(userRegionDTO1.getId());
        assertThat(userRegionDTO1).isEqualTo(userRegionDTO2);
        userRegionDTO2.setId(2L);
        assertThat(userRegionDTO1).isNotEqualTo(userRegionDTO2);
        userRegionDTO1.setId(null);
        assertThat(userRegionDTO1).isNotEqualTo(userRegionDTO2);
    }
}
