package erp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import erp.web.rest.TestUtil;

public class ShippersDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShippersDTO.class);
        ShippersDTO shippersDTO1 = new ShippersDTO();
        shippersDTO1.setId(1L);
        ShippersDTO shippersDTO2 = new ShippersDTO();
        assertThat(shippersDTO1).isNotEqualTo(shippersDTO2);
        shippersDTO2.setId(shippersDTO1.getId());
        assertThat(shippersDTO1).isEqualTo(shippersDTO2);
        shippersDTO2.setId(2L);
        assertThat(shippersDTO1).isNotEqualTo(shippersDTO2);
        shippersDTO1.setId(null);
        assertThat(shippersDTO1).isNotEqualTo(shippersDTO2);
    }
}
