package erp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TerritoryMapperTest {

    private TerritoryMapper territoryMapper;

    @BeforeEach
    public void setUp() {
        territoryMapper = new TerritoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(territoryMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(territoryMapper.fromId(null)).isNull();
    }
}
