package erp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UserRegionMapperTest {

    private UserRegionMapper userRegionMapper;

    @BeforeEach
    public void setUp() {
        userRegionMapper = new UserRegionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(userRegionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(userRegionMapper.fromId(null)).isNull();
    }
}
