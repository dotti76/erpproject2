package erp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OrderDetailsMapperTest {

    private OrderDetailsMapper orderDetailsMapper;

    @BeforeEach
    public void setUp() {
        orderDetailsMapper = new OrderDetailsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(orderDetailsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(orderDetailsMapper.fromId(null)).isNull();
    }
}
