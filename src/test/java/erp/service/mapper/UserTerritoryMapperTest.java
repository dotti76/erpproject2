package erp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UserTerritoryMapperTest {

    private UserTerritoryMapper userTerritoryMapper;

    @BeforeEach
    public void setUp() {
        userTerritoryMapper = new UserTerritoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(userTerritoryMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(userTerritoryMapper.fromId(null)).isNull();
    }
}
