package erp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ShippersMapperTest {

    private ShippersMapper shippersMapper;

    @BeforeEach
    public void setUp() {
        shippersMapper = new ShippersMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(shippersMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(shippersMapper.fromId(null)).isNull();
    }
}
