package erp.web.rest;

import erp.ErpProject2App;
import erp.domain.Territory;
import erp.repository.TerritoryRepository;
import erp.service.TerritoryService;
import erp.service.dto.TerritoryDTO;
import erp.service.mapper.TerritoryMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TerritoryResource} REST controller.
 */
@SpringBootTest(classes = ErpProject2App.class)
@AutoConfigureMockMvc
@WithMockUser
public class TerritoryResourceIT {

    private static final Long DEFAULT_REGION_ID = 1L;
    private static final Long UPDATED_REGION_ID = 2L;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TerritoryRepository territoryRepository;

    @Autowired
    private TerritoryMapper territoryMapper;

    @Autowired
    private TerritoryService territoryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTerritoryMockMvc;

    private Territory territory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Territory createEntity(EntityManager em) {
        Territory territory = new Territory()
            .regionID(DEFAULT_REGION_ID)
            .name(DEFAULT_NAME);
        return territory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Territory createUpdatedEntity(EntityManager em) {
        Territory territory = new Territory()
            .regionID(UPDATED_REGION_ID)
            .name(UPDATED_NAME);
        return territory;
    }

    @BeforeEach
    public void initTest() {
        territory = createEntity(em);
    }

    @Test
    @Transactional
    public void createTerritory() throws Exception {
        int databaseSizeBeforeCreate = territoryRepository.findAll().size();
        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);
        restTerritoryMockMvc.perform(post("/api/territories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isCreated());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeCreate + 1);
        Territory testTerritory = territoryList.get(territoryList.size() - 1);
        assertThat(testTerritory.getRegionID()).isEqualTo(DEFAULT_REGION_ID);
        assertThat(testTerritory.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createTerritoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = territoryRepository.findAll().size();

        // Create the Territory with an existing ID
        territory.setId(1L);
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTerritoryMockMvc.perform(post("/api/territories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTerritories() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList
        restTerritoryMockMvc.perform(get("/api/territories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(territory.getId().intValue())))
            .andExpect(jsonPath("$.[*].regionID").value(hasItem(DEFAULT_REGION_ID.intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getTerritory() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get the territory
        restTerritoryMockMvc.perform(get("/api/territories/{id}", territory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(territory.getId().intValue()))
            .andExpect(jsonPath("$.regionID").value(DEFAULT_REGION_ID.intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingTerritory() throws Exception {
        // Get the territory
        restTerritoryMockMvc.perform(get("/api/territories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTerritory() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();

        // Update the territory
        Territory updatedTerritory = territoryRepository.findById(territory.getId()).get();
        // Disconnect from session so that the updates on updatedTerritory are not directly saved in db
        em.detach(updatedTerritory);
        updatedTerritory
            .regionID(UPDATED_REGION_ID)
            .name(UPDATED_NAME);
        TerritoryDTO territoryDTO = territoryMapper.toDto(updatedTerritory);

        restTerritoryMockMvc.perform(put("/api/territories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isOk());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
        Territory testTerritory = territoryList.get(territoryList.size() - 1);
        assertThat(testTerritory.getRegionID()).isEqualTo(UPDATED_REGION_ID);
        assertThat(testTerritory.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTerritory() throws Exception {
        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();

        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTerritoryMockMvc.perform(put("/api/territories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTerritory() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        int databaseSizeBeforeDelete = territoryRepository.findAll().size();

        // Delete the territory
        restTerritoryMockMvc.perform(delete("/api/territories/{id}", territory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
