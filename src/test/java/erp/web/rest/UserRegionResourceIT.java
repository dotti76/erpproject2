package erp.web.rest;

import erp.ErpProject2App;
import erp.domain.UserRegion;
import erp.repository.UserRegionRepository;
import erp.service.UserRegionService;
import erp.service.dto.UserRegionDTO;
import erp.service.mapper.UserRegionMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserRegionResource} REST controller.
 */
@SpringBootTest(classes = ErpProject2App.class)
@AutoConfigureMockMvc
@WithMockUser
public class UserRegionResourceIT {

    private static final Long DEFAULT_REGION_ID = 1L;
    private static final Long UPDATED_REGION_ID = 2L;

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    @Autowired
    private UserRegionRepository userRegionRepository;

    @Autowired
    private UserRegionMapper userRegionMapper;

    @Autowired
    private UserRegionService userRegionService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserRegionMockMvc;

    private UserRegion userRegion;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserRegion createEntity(EntityManager em) {
        UserRegion userRegion = new UserRegion()
            .regionID(DEFAULT_REGION_ID)
            .userId(DEFAULT_USER_ID);
        return userRegion;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserRegion createUpdatedEntity(EntityManager em) {
        UserRegion userRegion = new UserRegion()
            .regionID(UPDATED_REGION_ID)
            .userId(UPDATED_USER_ID);
        return userRegion;
    }

    @BeforeEach
    public void initTest() {
        userRegion = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserRegion() throws Exception {
        int databaseSizeBeforeCreate = userRegionRepository.findAll().size();
        // Create the UserRegion
        UserRegionDTO userRegionDTO = userRegionMapper.toDto(userRegion);
        restUserRegionMockMvc.perform(post("/api/user-regions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userRegionDTO)))
            .andExpect(status().isCreated());

        // Validate the UserRegion in the database
        List<UserRegion> userRegionList = userRegionRepository.findAll();
        assertThat(userRegionList).hasSize(databaseSizeBeforeCreate + 1);
        UserRegion testUserRegion = userRegionList.get(userRegionList.size() - 1);
        assertThat(testUserRegion.getRegionID()).isEqualTo(DEFAULT_REGION_ID);
        assertThat(testUserRegion.getUserId()).isEqualTo(DEFAULT_USER_ID);
    }

    @Test
    @Transactional
    public void createUserRegionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userRegionRepository.findAll().size();

        // Create the UserRegion with an existing ID
        userRegion.setId(1L);
        UserRegionDTO userRegionDTO = userRegionMapper.toDto(userRegion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserRegionMockMvc.perform(post("/api/user-regions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userRegionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserRegion in the database
        List<UserRegion> userRegionList = userRegionRepository.findAll();
        assertThat(userRegionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserRegions() throws Exception {
        // Initialize the database
        userRegionRepository.saveAndFlush(userRegion);

        // Get all the userRegionList
        restUserRegionMockMvc.perform(get("/api/user-regions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userRegion.getId().intValue())))
            .andExpect(jsonPath("$.[*].regionID").value(hasItem(DEFAULT_REGION_ID.intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getUserRegion() throws Exception {
        // Initialize the database
        userRegionRepository.saveAndFlush(userRegion);

        // Get the userRegion
        restUserRegionMockMvc.perform(get("/api/user-regions/{id}", userRegion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userRegion.getId().intValue()))
            .andExpect(jsonPath("$.regionID").value(DEFAULT_REGION_ID.intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingUserRegion() throws Exception {
        // Get the userRegion
        restUserRegionMockMvc.perform(get("/api/user-regions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserRegion() throws Exception {
        // Initialize the database
        userRegionRepository.saveAndFlush(userRegion);

        int databaseSizeBeforeUpdate = userRegionRepository.findAll().size();

        // Update the userRegion
        UserRegion updatedUserRegion = userRegionRepository.findById(userRegion.getId()).get();
        // Disconnect from session so that the updates on updatedUserRegion are not directly saved in db
        em.detach(updatedUserRegion);
        updatedUserRegion
            .regionID(UPDATED_REGION_ID)
            .userId(UPDATED_USER_ID);
        UserRegionDTO userRegionDTO = userRegionMapper.toDto(updatedUserRegion);

        restUserRegionMockMvc.perform(put("/api/user-regions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userRegionDTO)))
            .andExpect(status().isOk());

        // Validate the UserRegion in the database
        List<UserRegion> userRegionList = userRegionRepository.findAll();
        assertThat(userRegionList).hasSize(databaseSizeBeforeUpdate);
        UserRegion testUserRegion = userRegionList.get(userRegionList.size() - 1);
        assertThat(testUserRegion.getRegionID()).isEqualTo(UPDATED_REGION_ID);
        assertThat(testUserRegion.getUserId()).isEqualTo(UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingUserRegion() throws Exception {
        int databaseSizeBeforeUpdate = userRegionRepository.findAll().size();

        // Create the UserRegion
        UserRegionDTO userRegionDTO = userRegionMapper.toDto(userRegion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserRegionMockMvc.perform(put("/api/user-regions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userRegionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserRegion in the database
        List<UserRegion> userRegionList = userRegionRepository.findAll();
        assertThat(userRegionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserRegion() throws Exception {
        // Initialize the database
        userRegionRepository.saveAndFlush(userRegion);

        int databaseSizeBeforeDelete = userRegionRepository.findAll().size();

        // Delete the userRegion
        restUserRegionMockMvc.perform(delete("/api/user-regions/{id}", userRegion.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserRegion> userRegionList = userRegionRepository.findAll();
        assertThat(userRegionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
