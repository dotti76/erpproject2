package erp.web.rest;

import erp.ErpProject2App;
import erp.domain.UserTerritory;
import erp.repository.UserTerritoryRepository;
import erp.service.UserTerritoryService;
import erp.service.dto.UserTerritoryDTO;
import erp.service.mapper.UserTerritoryMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserTerritoryResource} REST controller.
 */
@SpringBootTest(classes = ErpProject2App.class)
@AutoConfigureMockMvc
@WithMockUser
public class UserTerritoryResourceIT {

    private static final Long DEFAULT_TERRITORY_ID = 1L;
    private static final Long UPDATED_TERRITORY_ID = 2L;

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    @Autowired
    private UserTerritoryRepository userTerritoryRepository;

    @Autowired
    private UserTerritoryMapper userTerritoryMapper;

    @Autowired
    private UserTerritoryService userTerritoryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserTerritoryMockMvc;

    private UserTerritory userTerritory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserTerritory createEntity(EntityManager em) {
        UserTerritory userTerritory = new UserTerritory()
            .territoryId(DEFAULT_TERRITORY_ID)
            .userId(DEFAULT_USER_ID);
        return userTerritory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserTerritory createUpdatedEntity(EntityManager em) {
        UserTerritory userTerritory = new UserTerritory()
            .territoryId(UPDATED_TERRITORY_ID)
            .userId(UPDATED_USER_ID);
        return userTerritory;
    }

    @BeforeEach
    public void initTest() {
        userTerritory = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserTerritory() throws Exception {
        int databaseSizeBeforeCreate = userTerritoryRepository.findAll().size();
        // Create the UserTerritory
        UserTerritoryDTO userTerritoryDTO = userTerritoryMapper.toDto(userTerritory);
        restUserTerritoryMockMvc.perform(post("/api/user-territories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTerritoryDTO)))
            .andExpect(status().isCreated());

        // Validate the UserTerritory in the database
        List<UserTerritory> userTerritoryList = userTerritoryRepository.findAll();
        assertThat(userTerritoryList).hasSize(databaseSizeBeforeCreate + 1);
        UserTerritory testUserTerritory = userTerritoryList.get(userTerritoryList.size() - 1);
        assertThat(testUserTerritory.getTerritoryId()).isEqualTo(DEFAULT_TERRITORY_ID);
        assertThat(testUserTerritory.getUserId()).isEqualTo(DEFAULT_USER_ID);
    }

    @Test
    @Transactional
    public void createUserTerritoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userTerritoryRepository.findAll().size();

        // Create the UserTerritory with an existing ID
        userTerritory.setId(1L);
        UserTerritoryDTO userTerritoryDTO = userTerritoryMapper.toDto(userTerritory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserTerritoryMockMvc.perform(post("/api/user-territories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTerritoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserTerritory in the database
        List<UserTerritory> userTerritoryList = userTerritoryRepository.findAll();
        assertThat(userTerritoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserTerritories() throws Exception {
        // Initialize the database
        userTerritoryRepository.saveAndFlush(userTerritory);

        // Get all the userTerritoryList
        restUserTerritoryMockMvc.perform(get("/api/user-territories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userTerritory.getId().intValue())))
            .andExpect(jsonPath("$.[*].territoryId").value(hasItem(DEFAULT_TERRITORY_ID.intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getUserTerritory() throws Exception {
        // Initialize the database
        userTerritoryRepository.saveAndFlush(userTerritory);

        // Get the userTerritory
        restUserTerritoryMockMvc.perform(get("/api/user-territories/{id}", userTerritory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userTerritory.getId().intValue()))
            .andExpect(jsonPath("$.territoryId").value(DEFAULT_TERRITORY_ID.intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingUserTerritory() throws Exception {
        // Get the userTerritory
        restUserTerritoryMockMvc.perform(get("/api/user-territories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserTerritory() throws Exception {
        // Initialize the database
        userTerritoryRepository.saveAndFlush(userTerritory);

        int databaseSizeBeforeUpdate = userTerritoryRepository.findAll().size();

        // Update the userTerritory
        UserTerritory updatedUserTerritory = userTerritoryRepository.findById(userTerritory.getId()).get();
        // Disconnect from session so that the updates on updatedUserTerritory are not directly saved in db
        em.detach(updatedUserTerritory);
        updatedUserTerritory
            .territoryId(UPDATED_TERRITORY_ID)
            .userId(UPDATED_USER_ID);
        UserTerritoryDTO userTerritoryDTO = userTerritoryMapper.toDto(updatedUserTerritory);

        restUserTerritoryMockMvc.perform(put("/api/user-territories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTerritoryDTO)))
            .andExpect(status().isOk());

        // Validate the UserTerritory in the database
        List<UserTerritory> userTerritoryList = userTerritoryRepository.findAll();
        assertThat(userTerritoryList).hasSize(databaseSizeBeforeUpdate);
        UserTerritory testUserTerritory = userTerritoryList.get(userTerritoryList.size() - 1);
        assertThat(testUserTerritory.getTerritoryId()).isEqualTo(UPDATED_TERRITORY_ID);
        assertThat(testUserTerritory.getUserId()).isEqualTo(UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingUserTerritory() throws Exception {
        int databaseSizeBeforeUpdate = userTerritoryRepository.findAll().size();

        // Create the UserTerritory
        UserTerritoryDTO userTerritoryDTO = userTerritoryMapper.toDto(userTerritory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserTerritoryMockMvc.perform(put("/api/user-territories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTerritoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserTerritory in the database
        List<UserTerritory> userTerritoryList = userTerritoryRepository.findAll();
        assertThat(userTerritoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserTerritory() throws Exception {
        // Initialize the database
        userTerritoryRepository.saveAndFlush(userTerritory);

        int databaseSizeBeforeDelete = userTerritoryRepository.findAll().size();

        // Delete the userTerritory
        restUserTerritoryMockMvc.perform(delete("/api/user-territories/{id}", userTerritory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserTerritory> userTerritoryList = userTerritoryRepository.findAll();
        assertThat(userTerritoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
