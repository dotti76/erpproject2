package erp.web.rest;

import erp.ErpProject2App;
import erp.domain.Orders;
import erp.repository.OrdersRepository;
import erp.service.OrdersService;
import erp.service.dto.OrdersDTO;
import erp.service.mapper.OrdersMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrdersResource} REST controller.
 */
@SpringBootTest(classes = ErpProject2App.class)
@AutoConfigureMockMvc
@WithMockUser
public class OrdersResourceIT {

    private static final Long DEFAULT_CUSTOMER_ID = 1L;
    private static final Long UPDATED_CUSTOMER_ID = 2L;

    private static final Long DEFAULT_EMPLOYEE_ID = 1L;
    private static final Long UPDATED_EMPLOYEE_ID = 2L;

    private static final Instant DEFAULT_ORDER_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ORDER_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_SHIPPED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_SHIPPED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_SHIP_VIA = 1L;
    private static final Long UPDATED_SHIP_VIA = 2L;

    private static final String DEFAULT_SHIP_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_SHIP_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_SHIP_CITY = "AAAAAAAAAA";
    private static final String UPDATED_SHIP_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_FREIGHT = "AAAAAAAAAA";
    private static final String UPDATED_FREIGHT = "BBBBBBBBBB";

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrdersMockMvc;

    private Orders orders;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Orders createEntity(EntityManager em) {
        Orders orders = new Orders()
            .customerID(DEFAULT_CUSTOMER_ID)
            .employeeID(DEFAULT_EMPLOYEE_ID)
            .orderDate(DEFAULT_ORDER_DATE)
            .shippedDate(DEFAULT_SHIPPED_DATE)
            .shipVia(DEFAULT_SHIP_VIA)
            .shipAddress(DEFAULT_SHIP_ADDRESS)
            .shipCity(DEFAULT_SHIP_CITY)
            .freight(DEFAULT_FREIGHT);
        return orders;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Orders createUpdatedEntity(EntityManager em) {
        Orders orders = new Orders()
            .customerID(UPDATED_CUSTOMER_ID)
            .employeeID(UPDATED_EMPLOYEE_ID)
            .orderDate(UPDATED_ORDER_DATE)
            .shippedDate(UPDATED_SHIPPED_DATE)
            .shipVia(UPDATED_SHIP_VIA)
            .shipAddress(UPDATED_SHIP_ADDRESS)
            .shipCity(UPDATED_SHIP_CITY)
            .freight(UPDATED_FREIGHT);
        return orders;
    }

    @BeforeEach
    public void initTest() {
        orders = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrders() throws Exception {
        int databaseSizeBeforeCreate = ordersRepository.findAll().size();
        // Create the Orders
        OrdersDTO ordersDTO = ordersMapper.toDto(orders);
        restOrdersMockMvc.perform(post("/api/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ordersDTO)))
            .andExpect(status().isCreated());

        // Validate the Orders in the database
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeCreate + 1);
        Orders testOrders = ordersList.get(ordersList.size() - 1);
        assertThat(testOrders.getCustomerID()).isEqualTo(DEFAULT_CUSTOMER_ID);
        assertThat(testOrders.getEmployeeID()).isEqualTo(DEFAULT_EMPLOYEE_ID);
        assertThat(testOrders.getOrderDate()).isEqualTo(DEFAULT_ORDER_DATE);
        assertThat(testOrders.getShippedDate()).isEqualTo(DEFAULT_SHIPPED_DATE);
        assertThat(testOrders.getShipVia()).isEqualTo(DEFAULT_SHIP_VIA);
        assertThat(testOrders.getShipAddress()).isEqualTo(DEFAULT_SHIP_ADDRESS);
        assertThat(testOrders.getShipCity()).isEqualTo(DEFAULT_SHIP_CITY);
        assertThat(testOrders.getFreight()).isEqualTo(DEFAULT_FREIGHT);
    }

    @Test
    @Transactional
    public void createOrdersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ordersRepository.findAll().size();

        // Create the Orders with an existing ID
        orders.setId(1L);
        OrdersDTO ordersDTO = ordersMapper.toDto(orders);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrdersMockMvc.perform(post("/api/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ordersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Orders in the database
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrders() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList
        restOrdersMockMvc.perform(get("/api/orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orders.getId().intValue())))
            .andExpect(jsonPath("$.[*].customerID").value(hasItem(DEFAULT_CUSTOMER_ID.intValue())))
            .andExpect(jsonPath("$.[*].employeeID").value(hasItem(DEFAULT_EMPLOYEE_ID.intValue())))
            .andExpect(jsonPath("$.[*].orderDate").value(hasItem(DEFAULT_ORDER_DATE.toString())))
            .andExpect(jsonPath("$.[*].shippedDate").value(hasItem(DEFAULT_SHIPPED_DATE.toString())))
            .andExpect(jsonPath("$.[*].shipVia").value(hasItem(DEFAULT_SHIP_VIA.intValue())))
            .andExpect(jsonPath("$.[*].shipAddress").value(hasItem(DEFAULT_SHIP_ADDRESS)))
            .andExpect(jsonPath("$.[*].shipCity").value(hasItem(DEFAULT_SHIP_CITY)))
            .andExpect(jsonPath("$.[*].freight").value(hasItem(DEFAULT_FREIGHT)));
    }
    
    @Test
    @Transactional
    public void getOrders() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get the orders
        restOrdersMockMvc.perform(get("/api/orders/{id}", orders.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orders.getId().intValue()))
            .andExpect(jsonPath("$.customerID").value(DEFAULT_CUSTOMER_ID.intValue()))
            .andExpect(jsonPath("$.employeeID").value(DEFAULT_EMPLOYEE_ID.intValue()))
            .andExpect(jsonPath("$.orderDate").value(DEFAULT_ORDER_DATE.toString()))
            .andExpect(jsonPath("$.shippedDate").value(DEFAULT_SHIPPED_DATE.toString()))
            .andExpect(jsonPath("$.shipVia").value(DEFAULT_SHIP_VIA.intValue()))
            .andExpect(jsonPath("$.shipAddress").value(DEFAULT_SHIP_ADDRESS))
            .andExpect(jsonPath("$.shipCity").value(DEFAULT_SHIP_CITY))
            .andExpect(jsonPath("$.freight").value(DEFAULT_FREIGHT));
    }
    @Test
    @Transactional
    public void getNonExistingOrders() throws Exception {
        // Get the orders
        restOrdersMockMvc.perform(get("/api/orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrders() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        int databaseSizeBeforeUpdate = ordersRepository.findAll().size();

        // Update the orders
        Orders updatedOrders = ordersRepository.findById(orders.getId()).get();
        // Disconnect from session so that the updates on updatedOrders are not directly saved in db
        em.detach(updatedOrders);
        updatedOrders
            .customerID(UPDATED_CUSTOMER_ID)
            .employeeID(UPDATED_EMPLOYEE_ID)
            .orderDate(UPDATED_ORDER_DATE)
            .shippedDate(UPDATED_SHIPPED_DATE)
            .shipVia(UPDATED_SHIP_VIA)
            .shipAddress(UPDATED_SHIP_ADDRESS)
            .shipCity(UPDATED_SHIP_CITY)
            .freight(UPDATED_FREIGHT);
        OrdersDTO ordersDTO = ordersMapper.toDto(updatedOrders);

        restOrdersMockMvc.perform(put("/api/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ordersDTO)))
            .andExpect(status().isOk());

        // Validate the Orders in the database
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeUpdate);
        Orders testOrders = ordersList.get(ordersList.size() - 1);
        assertThat(testOrders.getCustomerID()).isEqualTo(UPDATED_CUSTOMER_ID);
        assertThat(testOrders.getEmployeeID()).isEqualTo(UPDATED_EMPLOYEE_ID);
        assertThat(testOrders.getOrderDate()).isEqualTo(UPDATED_ORDER_DATE);
        assertThat(testOrders.getShippedDate()).isEqualTo(UPDATED_SHIPPED_DATE);
        assertThat(testOrders.getShipVia()).isEqualTo(UPDATED_SHIP_VIA);
        assertThat(testOrders.getShipAddress()).isEqualTo(UPDATED_SHIP_ADDRESS);
        assertThat(testOrders.getShipCity()).isEqualTo(UPDATED_SHIP_CITY);
        assertThat(testOrders.getFreight()).isEqualTo(UPDATED_FREIGHT);
    }

    @Test
    @Transactional
    public void updateNonExistingOrders() throws Exception {
        int databaseSizeBeforeUpdate = ordersRepository.findAll().size();

        // Create the Orders
        OrdersDTO ordersDTO = ordersMapper.toDto(orders);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrdersMockMvc.perform(put("/api/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ordersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Orders in the database
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrders() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        int databaseSizeBeforeDelete = ordersRepository.findAll().size();

        // Delete the orders
        restOrdersMockMvc.perform(delete("/api/orders/{id}", orders.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
