package erp.web.rest;

import erp.ErpProject2App;
import erp.domain.Shippers;
import erp.repository.ShippersRepository;
import erp.service.ShippersService;
import erp.service.dto.ShippersDTO;
import erp.service.mapper.ShippersMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShippersResource} REST controller.
 */
@SpringBootTest(classes = ErpProject2App.class)
@AutoConfigureMockMvc
@WithMockUser
public class ShippersResourceIT {

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    @Autowired
    private ShippersRepository shippersRepository;

    @Autowired
    private ShippersMapper shippersMapper;

    @Autowired
    private ShippersService shippersService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restShippersMockMvc;

    private Shippers shippers;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Shippers createEntity(EntityManager em) {
        Shippers shippers = new Shippers()
            .companyName(DEFAULT_COMPANY_NAME)
            .phoneNumber(DEFAULT_PHONE_NUMBER);
        return shippers;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Shippers createUpdatedEntity(EntityManager em) {
        Shippers shippers = new Shippers()
            .companyName(UPDATED_COMPANY_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER);
        return shippers;
    }

    @BeforeEach
    public void initTest() {
        shippers = createEntity(em);
    }

    @Test
    @Transactional
    public void createShippers() throws Exception {
        int databaseSizeBeforeCreate = shippersRepository.findAll().size();
        // Create the Shippers
        ShippersDTO shippersDTO = shippersMapper.toDto(shippers);
        restShippersMockMvc.perform(post("/api/shippers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shippersDTO)))
            .andExpect(status().isCreated());

        // Validate the Shippers in the database
        List<Shippers> shippersList = shippersRepository.findAll();
        assertThat(shippersList).hasSize(databaseSizeBeforeCreate + 1);
        Shippers testShippers = shippersList.get(shippersList.size() - 1);
        assertThat(testShippers.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testShippers.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void createShippersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shippersRepository.findAll().size();

        // Create the Shippers with an existing ID
        shippers.setId(1L);
        ShippersDTO shippersDTO = shippersMapper.toDto(shippers);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShippersMockMvc.perform(post("/api/shippers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shippersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Shippers in the database
        List<Shippers> shippersList = shippersRepository.findAll();
        assertThat(shippersList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllShippers() throws Exception {
        // Initialize the database
        shippersRepository.saveAndFlush(shippers);

        // Get all the shippersList
        restShippersMockMvc.perform(get("/api/shippers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shippers.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)));
    }
    
    @Test
    @Transactional
    public void getShippers() throws Exception {
        // Initialize the database
        shippersRepository.saveAndFlush(shippers);

        // Get the shippers
        restShippersMockMvc.perform(get("/api/shippers/{id}", shippers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shippers.getId().intValue()))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER));
    }
    @Test
    @Transactional
    public void getNonExistingShippers() throws Exception {
        // Get the shippers
        restShippersMockMvc.perform(get("/api/shippers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShippers() throws Exception {
        // Initialize the database
        shippersRepository.saveAndFlush(shippers);

        int databaseSizeBeforeUpdate = shippersRepository.findAll().size();

        // Update the shippers
        Shippers updatedShippers = shippersRepository.findById(shippers.getId()).get();
        // Disconnect from session so that the updates on updatedShippers are not directly saved in db
        em.detach(updatedShippers);
        updatedShippers
            .companyName(UPDATED_COMPANY_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER);
        ShippersDTO shippersDTO = shippersMapper.toDto(updatedShippers);

        restShippersMockMvc.perform(put("/api/shippers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shippersDTO)))
            .andExpect(status().isOk());

        // Validate the Shippers in the database
        List<Shippers> shippersList = shippersRepository.findAll();
        assertThat(shippersList).hasSize(databaseSizeBeforeUpdate);
        Shippers testShippers = shippersList.get(shippersList.size() - 1);
        assertThat(testShippers.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testShippers.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void updateNonExistingShippers() throws Exception {
        int databaseSizeBeforeUpdate = shippersRepository.findAll().size();

        // Create the Shippers
        ShippersDTO shippersDTO = shippersMapper.toDto(shippers);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShippersMockMvc.perform(put("/api/shippers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shippersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Shippers in the database
        List<Shippers> shippersList = shippersRepository.findAll();
        assertThat(shippersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShippers() throws Exception {
        // Initialize the database
        shippersRepository.saveAndFlush(shippers);

        int databaseSizeBeforeDelete = shippersRepository.findAll().size();

        // Delete the shippers
        restShippersMockMvc.perform(delete("/api/shippers/{id}", shippers.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Shippers> shippersList = shippersRepository.findAll();
        assertThat(shippersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
