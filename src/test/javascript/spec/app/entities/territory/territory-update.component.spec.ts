import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ErpProject2TestModule } from '../../../test.module';
import { TerritoryUpdateComponent } from 'app/entities/territory/territory-update.component';
import { TerritoryService } from 'app/entities/territory/territory.service';
import { Territory } from 'app/shared/model/territory.model';

describe('Component Tests', () => {
  describe('Territory Management Update Component', () => {
    let comp: TerritoryUpdateComponent;
    let fixture: ComponentFixture<TerritoryUpdateComponent>;
    let service: TerritoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [TerritoryUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TerritoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TerritoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TerritoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Territory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Territory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
