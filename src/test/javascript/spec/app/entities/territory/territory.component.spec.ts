import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ErpProject2TestModule } from '../../../test.module';
import { TerritoryComponent } from 'app/entities/territory/territory.component';
import { TerritoryService } from 'app/entities/territory/territory.service';
import { Territory } from 'app/shared/model/territory.model';

describe('Component Tests', () => {
  describe('Territory Management Component', () => {
    let comp: TerritoryComponent;
    let fixture: ComponentFixture<TerritoryComponent>;
    let service: TerritoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [TerritoryComponent],
      })
        .overrideTemplate(TerritoryComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TerritoryComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TerritoryService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Territory(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.territories && comp.territories[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
