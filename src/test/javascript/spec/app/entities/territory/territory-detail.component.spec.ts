import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ErpProject2TestModule } from '../../../test.module';
import { TerritoryDetailComponent } from 'app/entities/territory/territory-detail.component';
import { Territory } from 'app/shared/model/territory.model';

describe('Component Tests', () => {
  describe('Territory Management Detail Component', () => {
    let comp: TerritoryDetailComponent;
    let fixture: ComponentFixture<TerritoryDetailComponent>;
    const route = ({ data: of({ territory: new Territory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [TerritoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TerritoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TerritoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load territory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.territory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
