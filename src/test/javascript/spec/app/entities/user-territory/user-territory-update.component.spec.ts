import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ErpProject2TestModule } from '../../../test.module';
import { UserTerritoryUpdateComponent } from 'app/entities/user-territory/user-territory-update.component';
import { UserTerritoryService } from 'app/entities/user-territory/user-territory.service';
import { UserTerritory } from 'app/shared/model/user-territory.model';

describe('Component Tests', () => {
  describe('UserTerritory Management Update Component', () => {
    let comp: UserTerritoryUpdateComponent;
    let fixture: ComponentFixture<UserTerritoryUpdateComponent>;
    let service: UserTerritoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [UserTerritoryUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(UserTerritoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserTerritoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserTerritoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserTerritory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserTerritory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
