import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ErpProject2TestModule } from '../../../test.module';
import { UserTerritoryComponent } from 'app/entities/user-territory/user-territory.component';
import { UserTerritoryService } from 'app/entities/user-territory/user-territory.service';
import { UserTerritory } from 'app/shared/model/user-territory.model';

describe('Component Tests', () => {
  describe('UserTerritory Management Component', () => {
    let comp: UserTerritoryComponent;
    let fixture: ComponentFixture<UserTerritoryComponent>;
    let service: UserTerritoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [UserTerritoryComponent],
      })
        .overrideTemplate(UserTerritoryComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserTerritoryComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserTerritoryService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new UserTerritory(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.userTerritories && comp.userTerritories[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
