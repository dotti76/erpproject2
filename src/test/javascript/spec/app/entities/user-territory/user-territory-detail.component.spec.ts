import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ErpProject2TestModule } from '../../../test.module';
import { UserTerritoryDetailComponent } from 'app/entities/user-territory/user-territory-detail.component';
import { UserTerritory } from 'app/shared/model/user-territory.model';

describe('Component Tests', () => {
  describe('UserTerritory Management Detail Component', () => {
    let comp: UserTerritoryDetailComponent;
    let fixture: ComponentFixture<UserTerritoryDetailComponent>;
    const route = ({ data: of({ userTerritory: new UserTerritory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [UserTerritoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(UserTerritoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserTerritoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load userTerritory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userTerritory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
