import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ErpProject2TestModule } from '../../../test.module';
import { UserRegionComponent } from 'app/entities/user-region/user-region.component';
import { UserRegionService } from 'app/entities/user-region/user-region.service';
import { UserRegion } from 'app/shared/model/user-region.model';

describe('Component Tests', () => {
  describe('UserRegion Management Component', () => {
    let comp: UserRegionComponent;
    let fixture: ComponentFixture<UserRegionComponent>;
    let service: UserRegionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [UserRegionComponent],
      })
        .overrideTemplate(UserRegionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserRegionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserRegionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new UserRegion(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.userRegions && comp.userRegions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
