import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ErpProject2TestModule } from '../../../test.module';
import { UserRegionDetailComponent } from 'app/entities/user-region/user-region-detail.component';
import { UserRegion } from 'app/shared/model/user-region.model';

describe('Component Tests', () => {
  describe('UserRegion Management Detail Component', () => {
    let comp: UserRegionDetailComponent;
    let fixture: ComponentFixture<UserRegionDetailComponent>;
    const route = ({ data: of({ userRegion: new UserRegion(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [UserRegionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(UserRegionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserRegionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load userRegion on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userRegion).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
