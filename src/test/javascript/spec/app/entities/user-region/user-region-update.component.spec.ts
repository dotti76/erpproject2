import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ErpProject2TestModule } from '../../../test.module';
import { UserRegionUpdateComponent } from 'app/entities/user-region/user-region-update.component';
import { UserRegionService } from 'app/entities/user-region/user-region.service';
import { UserRegion } from 'app/shared/model/user-region.model';

describe('Component Tests', () => {
  describe('UserRegion Management Update Component', () => {
    let comp: UserRegionUpdateComponent;
    let fixture: ComponentFixture<UserRegionUpdateComponent>;
    let service: UserRegionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ErpProject2TestModule],
        declarations: [UserRegionUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(UserRegionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserRegionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserRegionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserRegion(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserRegion();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
