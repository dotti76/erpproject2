import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRegion, Region } from 'app/shared/model/region.model';
import { RegionService } from './region.service';
import { IUserRegion } from 'app/shared/model/user-region.model';
import { UserRegionService } from 'app/entities/user-region/user-region.service';
import { ITerritory } from 'app/shared/model/territory.model';
import { TerritoryService } from 'app/entities/territory/territory.service';

type SelectableEntity = IUserRegion | ITerritory;

@Component({
  selector: 'jhi-region-update',
  templateUrl: './region-update.component.html',
})
export class RegionUpdateComponent implements OnInit {
  isSaving = false;
  userregions: IUserRegion[] = [];
  territories: ITerritory[] = [];

  editForm = this.fb.group({
    id: [],
    regionName: [],
    userRegionId: [],
    territoryId: [],
  });

  constructor(
    protected regionService: RegionService,
    protected userRegionService: UserRegionService,
    protected territoryService: TerritoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ region }) => {
      this.updateForm(region);

      this.userRegionService.query().subscribe((res: HttpResponse<IUserRegion[]>) => (this.userregions = res.body || []));

      this.territoryService.query().subscribe((res: HttpResponse<ITerritory[]>) => (this.territories = res.body || []));
    });
  }

  updateForm(region: IRegion): void {
    this.editForm.patchValue({
      id: region.id,
      regionName: region.regionName,
      userRegionId: region.userRegionId,
      territoryId: region.territoryId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const region = this.createFromForm();
    if (region.id !== undefined) {
      this.subscribeToSaveResponse(this.regionService.update(region));
    } else {
      this.subscribeToSaveResponse(this.regionService.create(region));
    }
  }

  private createFromForm(): IRegion {
    return {
      ...new Region(),
      id: this.editForm.get(['id'])!.value,
      regionName: this.editForm.get(['regionName'])!.value,
      userRegionId: this.editForm.get(['userRegionId'])!.value,
      territoryId: this.editForm.get(['territoryId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRegion>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
