import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOrderDetails, OrderDetails } from 'app/shared/model/order-details.model';
import { OrderDetailsService } from './order-details.service';

@Component({
  selector: 'jhi-order-details-update',
  templateUrl: './order-details-update.component.html',
})
export class OrderDetailsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    orderID: [],
    productID: [],
    unitPrice: [],
    quantity: [],
  });

  constructor(protected orderDetailsService: OrderDetailsService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderDetails }) => {
      this.updateForm(orderDetails);
    });
  }

  updateForm(orderDetails: IOrderDetails): void {
    this.editForm.patchValue({
      id: orderDetails.id,
      orderID: orderDetails.orderID,
      productID: orderDetails.productID,
      unitPrice: orderDetails.unitPrice,
      quantity: orderDetails.quantity,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orderDetails = this.createFromForm();
    if (orderDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.orderDetailsService.update(orderDetails));
    } else {
      this.subscribeToSaveResponse(this.orderDetailsService.create(orderDetails));
    }
  }

  private createFromForm(): IOrderDetails {
    return {
      ...new OrderDetails(),
      id: this.editForm.get(['id'])!.value,
      orderID: this.editForm.get(['orderID'])!.value,
      productID: this.editForm.get(['productID'])!.value,
      unitPrice: this.editForm.get(['unitPrice'])!.value,
      quantity: this.editForm.get(['quantity'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderDetails>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
