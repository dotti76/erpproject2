import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUserRegion } from 'app/shared/model/user-region.model';
import { UserRegionService } from './user-region.service';
import { UserRegionDeleteDialogComponent } from './user-region-delete-dialog.component';

@Component({
  selector: 'jhi-user-region',
  templateUrl: './user-region.component.html',
})
export class UserRegionComponent implements OnInit, OnDestroy {
  userRegions?: IUserRegion[];
  eventSubscriber?: Subscription;

  constructor(protected userRegionService: UserRegionService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.userRegionService.query().subscribe((res: HttpResponse<IUserRegion[]>) => (this.userRegions = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInUserRegions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUserRegion): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUserRegions(): void {
    this.eventSubscriber = this.eventManager.subscribe('userRegionListModification', () => this.loadAll());
  }

  delete(userRegion: IUserRegion): void {
    const modalRef = this.modalService.open(UserRegionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.userRegion = userRegion;
  }
}
