import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserRegion } from 'app/shared/model/user-region.model';

type EntityResponseType = HttpResponse<IUserRegion>;
type EntityArrayResponseType = HttpResponse<IUserRegion[]>;

@Injectable({ providedIn: 'root' })
export class UserRegionService {
  public resourceUrl = SERVER_API_URL + 'api/user-regions';

  constructor(protected http: HttpClient) {}

  create(userRegion: IUserRegion): Observable<EntityResponseType> {
    return this.http.post<IUserRegion>(this.resourceUrl, userRegion, { observe: 'response' });
  }

  update(userRegion: IUserRegion): Observable<EntityResponseType> {
    return this.http.put<IUserRegion>(this.resourceUrl, userRegion, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserRegion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserRegion[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
