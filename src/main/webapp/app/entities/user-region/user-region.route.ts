import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUserRegion, UserRegion } from 'app/shared/model/user-region.model';
import { UserRegionService } from './user-region.service';
import { UserRegionComponent } from './user-region.component';
import { UserRegionDetailComponent } from './user-region-detail.component';
import { UserRegionUpdateComponent } from './user-region-update.component';

@Injectable({ providedIn: 'root' })
export class UserRegionResolve implements Resolve<IUserRegion> {
  constructor(private service: UserRegionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserRegion> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((userRegion: HttpResponse<UserRegion>) => {
          if (userRegion.body) {
            return of(userRegion.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UserRegion());
  }
}

export const userRegionRoute: Routes = [
  {
    path: '',
    component: UserRegionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'UserRegions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: UserRegionDetailComponent,
    resolve: {
      userRegion: UserRegionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'UserRegions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: UserRegionUpdateComponent,
    resolve: {
      userRegion: UserRegionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'UserRegions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: UserRegionUpdateComponent,
    resolve: {
      userRegion: UserRegionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'UserRegions',
    },
    canActivate: [UserRouteAccessService],
  },
];
