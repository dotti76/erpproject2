import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ErpProject2SharedModule } from 'app/shared/shared.module';
import { UserRegionComponent } from './user-region.component';
import { UserRegionDetailComponent } from './user-region-detail.component';
import { UserRegionUpdateComponent } from './user-region-update.component';
import { UserRegionDeleteDialogComponent } from './user-region-delete-dialog.component';
import { userRegionRoute } from './user-region.route';

@NgModule({
  imports: [ErpProject2SharedModule, RouterModule.forChild(userRegionRoute)],
  declarations: [UserRegionComponent, UserRegionDetailComponent, UserRegionUpdateComponent, UserRegionDeleteDialogComponent],
  entryComponents: [UserRegionDeleteDialogComponent],
})
export class ErpProject2UserRegionModule {}
