import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserRegion } from 'app/shared/model/user-region.model';
import { UserRegionService } from './user-region.service';

@Component({
  templateUrl: './user-region-delete-dialog.component.html',
})
export class UserRegionDeleteDialogComponent {
  userRegion?: IUserRegion;

  constructor(
    protected userRegionService: UserRegionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.userRegionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('userRegionListModification');
      this.activeModal.close();
    });
  }
}
