import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserRegion } from 'app/shared/model/user-region.model';

@Component({
  selector: 'jhi-user-region-detail',
  templateUrl: './user-region-detail.component.html',
})
export class UserRegionDetailComponent implements OnInit {
  userRegion: IUserRegion | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userRegion }) => (this.userRegion = userRegion));
  }

  previousState(): void {
    window.history.back();
  }
}
