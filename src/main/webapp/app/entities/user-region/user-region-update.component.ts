import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserRegion, UserRegion } from 'app/shared/model/user-region.model';
import { UserRegionService } from './user-region.service';

@Component({
  selector: 'jhi-user-region-update',
  templateUrl: './user-region-update.component.html',
})
export class UserRegionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    regionID: [],
    userId: [],
  });

  constructor(protected userRegionService: UserRegionService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userRegion }) => {
      this.updateForm(userRegion);
    });
  }

  updateForm(userRegion: IUserRegion): void {
    this.editForm.patchValue({
      id: userRegion.id,
      regionID: userRegion.regionID,
      userId: userRegion.userId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userRegion = this.createFromForm();
    if (userRegion.id !== undefined) {
      this.subscribeToSaveResponse(this.userRegionService.update(userRegion));
    } else {
      this.subscribeToSaveResponse(this.userRegionService.create(userRegion));
    }
  }

  private createFromForm(): IUserRegion {
    return {
      ...new UserRegion(),
      id: this.editForm.get(['id'])!.value,
      regionID: this.editForm.get(['regionID'])!.value,
      userId: this.editForm.get(['userId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserRegion>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
