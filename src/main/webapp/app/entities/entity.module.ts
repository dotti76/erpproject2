import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'shippers',
        loadChildren: () => import('./shippers/shippers.module').then(m => m.ErpProject2ShippersModule),
      },
      {
        path: 'customers',
        loadChildren: () => import('./customers/customers.module').then(m => m.ErpProject2CustomersModule),
      },
      {
        path: 'categories',
        loadChildren: () => import('./categories/categories.module').then(m => m.ErpProject2CategoriesModule),
      },
      {
        path: 'order-details',
        loadChildren: () => import('./order-details/order-details.module').then(m => m.ErpProject2OrderDetailsModule),
      },
      {
        path: 'products',
        loadChildren: () => import('./products/products.module').then(m => m.ErpProject2ProductsModule),
      },
      {
        path: 'employee',
        loadChildren: () => import('./employee/employee.module').then(m => m.ErpProject2EmployeeModule),
      },
      {
        path: 'orders',
        loadChildren: () => import('./orders/orders.module').then(m => m.ErpProject2OrdersModule),
      },
      {
        path: 'suppliers',
        loadChildren: () => import('./suppliers/suppliers.module').then(m => m.ErpProject2SuppliersModule),
      },
      {
        path: 'region',
        loadChildren: () => import('./region/region.module').then(m => m.ErpProject2RegionModule),
      },
      {
        path: 'territory',
        loadChildren: () => import('./territory/territory.module').then(m => m.ErpProject2TerritoryModule),
      },
      {
        path: 'user-region',
        loadChildren: () => import('./user-region/user-region.module').then(m => m.ErpProject2UserRegionModule),
      },
      {
        path: 'user-territory',
        loadChildren: () => import('./user-territory/user-territory.module').then(m => m.ErpProject2UserTerritoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ErpProject2EntityModule {}
