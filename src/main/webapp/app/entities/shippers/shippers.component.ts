import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShippers } from 'app/shared/model/shippers.model';
import { ShippersService } from './shippers.service';
import { ShippersDeleteDialogComponent } from './shippers-delete-dialog.component';

@Component({
  selector: 'jhi-shippers',
  templateUrl: './shippers.component.html',
})
export class ShippersComponent implements OnInit, OnDestroy {
  shippers?: IShippers[];
  eventSubscriber?: Subscription;

  constructor(protected shippersService: ShippersService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.shippersService.query().subscribe((res: HttpResponse<IShippers[]>) => (this.shippers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShippers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShippers): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShippers(): void {
    this.eventSubscriber = this.eventManager.subscribe('shippersListModification', () => this.loadAll());
  }

  delete(shippers: IShippers): void {
    const modalRef = this.modalService.open(ShippersDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shippers = shippers;
  }
}
