import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IShippers, Shippers } from 'app/shared/model/shippers.model';
import { ShippersService } from './shippers.service';
import { IOrders } from 'app/shared/model/orders.model';
import { OrdersService } from 'app/entities/orders/orders.service';

@Component({
  selector: 'jhi-shippers-update',
  templateUrl: './shippers-update.component.html',
})
export class ShippersUpdateComponent implements OnInit {
  isSaving = false;
  orders: IOrders[] = [];

  editForm = this.fb.group({
    id: [],
    companyName: [],
    phoneNumber: [],
    ordersId: [],
  });

  constructor(
    protected shippersService: ShippersService,
    protected ordersService: OrdersService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shippers }) => {
      this.updateForm(shippers);

      this.ordersService.query().subscribe((res: HttpResponse<IOrders[]>) => (this.orders = res.body || []));
    });
  }

  updateForm(shippers: IShippers): void {
    this.editForm.patchValue({
      id: shippers.id,
      companyName: shippers.companyName,
      phoneNumber: shippers.phoneNumber,
      ordersId: shippers.ordersId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shippers = this.createFromForm();
    if (shippers.id !== undefined) {
      this.subscribeToSaveResponse(this.shippersService.update(shippers));
    } else {
      this.subscribeToSaveResponse(this.shippersService.create(shippers));
    }
  }

  private createFromForm(): IShippers {
    return {
      ...new Shippers(),
      id: this.editForm.get(['id'])!.value,
      companyName: this.editForm.get(['companyName'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      ordersId: this.editForm.get(['ordersId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShippers>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOrders): any {
    return item.id;
  }
}
