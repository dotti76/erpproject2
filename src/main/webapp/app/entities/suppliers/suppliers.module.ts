import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ErpProject2SharedModule } from 'app/shared/shared.module';
import { SuppliersComponent } from './suppliers.component';
import { SuppliersDetailComponent } from './suppliers-detail.component';
import { SuppliersUpdateComponent } from './suppliers-update.component';
import { SuppliersDeleteDialogComponent } from './suppliers-delete-dialog.component';
import { suppliersRoute } from './suppliers.route';

@NgModule({
  imports: [ErpProject2SharedModule, RouterModule.forChild(suppliersRoute)],
  declarations: [SuppliersComponent, SuppliersDetailComponent, SuppliersUpdateComponent, SuppliersDeleteDialogComponent],
  entryComponents: [SuppliersDeleteDialogComponent],
})
export class ErpProject2SuppliersModule {}
