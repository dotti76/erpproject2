import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISuppliers } from 'app/shared/model/suppliers.model';

type EntityResponseType = HttpResponse<ISuppliers>;
type EntityArrayResponseType = HttpResponse<ISuppliers[]>;

@Injectable({ providedIn: 'root' })
export class SuppliersService {
  public resourceUrl = SERVER_API_URL + 'api/suppliers';

  constructor(protected http: HttpClient) {}

  create(suppliers: ISuppliers): Observable<EntityResponseType> {
    return this.http.post<ISuppliers>(this.resourceUrl, suppliers, { observe: 'response' });
  }

  update(suppliers: ISuppliers): Observable<EntityResponseType> {
    return this.http.put<ISuppliers>(this.resourceUrl, suppliers, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISuppliers>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISuppliers[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
