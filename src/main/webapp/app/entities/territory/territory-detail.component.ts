import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITerritory } from 'app/shared/model/territory.model';

@Component({
  selector: 'jhi-territory-detail',
  templateUrl: './territory-detail.component.html',
})
export class TerritoryDetailComponent implements OnInit {
  territory: ITerritory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ territory }) => (this.territory = territory));
  }

  previousState(): void {
    window.history.back();
  }
}
