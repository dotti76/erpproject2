import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ErpProject2SharedModule } from 'app/shared/shared.module';
import { TerritoryComponent } from './territory.component';
import { TerritoryDetailComponent } from './territory-detail.component';
import { TerritoryUpdateComponent } from './territory-update.component';
import { TerritoryDeleteDialogComponent } from './territory-delete-dialog.component';
import { territoryRoute } from './territory.route';

@NgModule({
  imports: [ErpProject2SharedModule, RouterModule.forChild(territoryRoute)],
  declarations: [TerritoryComponent, TerritoryDetailComponent, TerritoryUpdateComponent, TerritoryDeleteDialogComponent],
  entryComponents: [TerritoryDeleteDialogComponent],
})
export class ErpProject2TerritoryModule {}
