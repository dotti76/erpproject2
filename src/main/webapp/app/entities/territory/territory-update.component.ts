import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITerritory, Territory } from 'app/shared/model/territory.model';
import { TerritoryService } from './territory.service';

@Component({
  selector: 'jhi-territory-update',
  templateUrl: './territory-update.component.html',
})
export class TerritoryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    regionID: [],
    name: [],
  });

  constructor(protected territoryService: TerritoryService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ territory }) => {
      this.updateForm(territory);
    });
  }

  updateForm(territory: ITerritory): void {
    this.editForm.patchValue({
      id: territory.id,
      regionID: territory.regionID,
      name: territory.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const territory = this.createFromForm();
    if (territory.id !== undefined) {
      this.subscribeToSaveResponse(this.territoryService.update(territory));
    } else {
      this.subscribeToSaveResponse(this.territoryService.create(territory));
    }
  }

  private createFromForm(): ITerritory {
    return {
      ...new Territory(),
      id: this.editForm.get(['id'])!.value,
      regionID: this.editForm.get(['regionID'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITerritory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
