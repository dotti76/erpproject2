import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITerritory } from 'app/shared/model/territory.model';
import { TerritoryService } from './territory.service';
import { TerritoryDeleteDialogComponent } from './territory-delete-dialog.component';

@Component({
  selector: 'jhi-territory',
  templateUrl: './territory.component.html',
})
export class TerritoryComponent implements OnInit, OnDestroy {
  territories?: ITerritory[];
  eventSubscriber?: Subscription;

  constructor(protected territoryService: TerritoryService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.territoryService.query().subscribe((res: HttpResponse<ITerritory[]>) => (this.territories = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTerritories();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITerritory): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTerritories(): void {
    this.eventSubscriber = this.eventManager.subscribe('territoryListModification', () => this.loadAll());
  }

  delete(territory: ITerritory): void {
    const modalRef = this.modalService.open(TerritoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.territory = territory;
  }

  weather(territory: ITerritory): void {
    this.territoryService.weather(territory.id!).subscribe(
      () => this.onWeatherSuccess(),
      () => this.onWeatherError()
    );
  }

  protected onWeatherSuccess(): void {
    //    console.log("weather success");
  }

  protected onWeatherError(): void {
    //    console.log("weather error");
  }
}
