import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITerritory } from 'app/shared/model/territory.model';

type EntityResponseType = HttpResponse<ITerritory>;
type EntityArrayResponseType = HttpResponse<ITerritory[]>;

@Injectable({ providedIn: 'root' })
export class TerritoryService {
  public resourceUrl = SERVER_API_URL + 'api/territories';

  constructor(protected http: HttpClient) {}

  create(territory: ITerritory): Observable<EntityResponseType> {
    return this.http.post<ITerritory>(this.resourceUrl, territory, { observe: 'response' });
  }

  update(territory: ITerritory): Observable<EntityResponseType> {
    return this.http.put<ITerritory>(this.resourceUrl, territory, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITerritory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITerritory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  weather(id: number): Observable<EntityResponseType> {
    return this.http.get<ITerritory>(`${this.resourceUrl}/weather/${id}`, { observe: 'response' });
  }
}
