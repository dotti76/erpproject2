import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITerritory, Territory } from 'app/shared/model/territory.model';
import { TerritoryService } from './territory.service';
import { TerritoryComponent } from './territory.component';
import { TerritoryDetailComponent } from './territory-detail.component';
import { TerritoryUpdateComponent } from './territory-update.component';

@Injectable({ providedIn: 'root' })
export class TerritoryResolve implements Resolve<ITerritory> {
  constructor(private service: TerritoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITerritory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((territory: HttpResponse<Territory>) => {
          if (territory.body) {
            return of(territory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Territory());
  }
}

export const territoryRoute: Routes = [
  {
    path: '',
    component: TerritoryComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Territories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TerritoryDetailComponent,
    resolve: {
      territory: TerritoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Territories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TerritoryUpdateComponent,
    resolve: {
      territory: TerritoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Territories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TerritoryUpdateComponent,
    resolve: {
      territory: TerritoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Territories',
    },
    canActivate: [UserRouteAccessService],
  },
];
