import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITerritory } from 'app/shared/model/territory.model';
import { TerritoryService } from './territory.service';

@Component({
  templateUrl: './territory-delete-dialog.component.html',
})
export class TerritoryDeleteDialogComponent {
  territory?: ITerritory;

  constructor(protected territoryService: TerritoryService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.territoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('territoryListModification');
      this.activeModal.close();
    });
  }
}
