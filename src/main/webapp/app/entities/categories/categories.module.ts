import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ErpProject2SharedModule } from 'app/shared/shared.module';
import { CategoriesComponent } from './categories.component';
import { CategoriesDetailComponent } from './categories-detail.component';
import { CategoriesUpdateComponent } from './categories-update.component';
import { CategoriesDeleteDialogComponent } from './categories-delete-dialog.component';
import { categoriesRoute } from './categories.route';

@NgModule({
  imports: [ErpProject2SharedModule, RouterModule.forChild(categoriesRoute)],
  declarations: [CategoriesComponent, CategoriesDetailComponent, CategoriesUpdateComponent, CategoriesDeleteDialogComponent],
  entryComponents: [CategoriesDeleteDialogComponent],
})
export class ErpProject2CategoriesModule {}
