import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IOrders, Orders } from 'app/shared/model/orders.model';
import { OrdersService } from './orders.service';
import { IOrderDetails } from 'app/shared/model/order-details.model';
import { OrderDetailsService } from 'app/entities/order-details/order-details.service';

@Component({
  selector: 'jhi-orders-update',
  templateUrl: './orders-update.component.html',
})
export class OrdersUpdateComponent implements OnInit {
  isSaving = false;
  orderdetails: IOrderDetails[] = [];

  editForm = this.fb.group({
    id: [],
    customerID: [],
    employeeID: [],
    orderDate: [],
    shippedDate: [],
    shipVia: [],
    shipAddress: [],
    shipCity: [],
    freight: [],
    orderDetailsId: [],
  });

  constructor(
    protected ordersService: OrdersService,
    protected orderDetailsService: OrderDetailsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orders }) => {
      if (!orders.id) {
        const today = moment().startOf('day');
        orders.orderDate = today;
        orders.shippedDate = today;
      }

      this.updateForm(orders);

      this.orderDetailsService.query().subscribe((res: HttpResponse<IOrderDetails[]>) => (this.orderdetails = res.body || []));
    });
  }

  updateForm(orders: IOrders): void {
    this.editForm.patchValue({
      id: orders.id,
      customerID: orders.customerID,
      employeeID: orders.employeeID,
      orderDate: orders.orderDate ? orders.orderDate.format(DATE_TIME_FORMAT) : null,
      shippedDate: orders.shippedDate ? orders.shippedDate.format(DATE_TIME_FORMAT) : null,
      shipVia: orders.shipVia,
      shipAddress: orders.shipAddress,
      shipCity: orders.shipCity,
      freight: orders.freight,
      orderDetailsId: orders.orderDetailsId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orders = this.createFromForm();
    if (orders.id !== undefined) {
      this.subscribeToSaveResponse(this.ordersService.update(orders));
    } else {
      this.subscribeToSaveResponse(this.ordersService.create(orders));
    }
  }

  private createFromForm(): IOrders {
    return {
      ...new Orders(),
      id: this.editForm.get(['id'])!.value,
      customerID: this.editForm.get(['customerID'])!.value,
      employeeID: this.editForm.get(['employeeID'])!.value,
      orderDate: this.editForm.get(['orderDate'])!.value ? moment(this.editForm.get(['orderDate'])!.value, DATE_TIME_FORMAT) : undefined,
      shippedDate: this.editForm.get(['shippedDate'])!.value
        ? moment(this.editForm.get(['shippedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      shipVia: this.editForm.get(['shipVia'])!.value,
      shipAddress: this.editForm.get(['shipAddress'])!.value,
      shipCity: this.editForm.get(['shipCity'])!.value,
      freight: this.editForm.get(['freight'])!.value,
      orderDetailsId: this.editForm.get(['orderDetailsId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrders>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOrderDetails): any {
    return item.id;
  }
}
