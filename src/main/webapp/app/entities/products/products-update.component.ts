import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProducts, Products } from 'app/shared/model/products.model';
import { ProductsService } from './products.service';
import { IOrderDetails } from 'app/shared/model/order-details.model';
import { OrderDetailsService } from 'app/entities/order-details/order-details.service';

@Component({
  selector: 'jhi-products-update',
  templateUrl: './products-update.component.html',
})
export class ProductsUpdateComponent implements OnInit {
  isSaving = false;
  orderdetails: IOrderDetails[] = [];

  editForm = this.fb.group({
    id: [],
    productName: [],
    categoryID: [],
    supplierID: [],
    unitPrice: [],
    orderDetailsId: [],
  });

  constructor(
    protected productsService: ProductsService,
    protected orderDetailsService: OrderDetailsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ products }) => {
      this.updateForm(products);

      this.orderDetailsService.query().subscribe((res: HttpResponse<IOrderDetails[]>) => (this.orderdetails = res.body || []));
    });
  }

  updateForm(products: IProducts): void {
    this.editForm.patchValue({
      id: products.id,
      productName: products.productName,
      categoryID: products.categoryID,
      supplierID: products.supplierID,
      unitPrice: products.unitPrice,
      orderDetailsId: products.orderDetailsId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const products = this.createFromForm();
    if (products.id !== undefined) {
      this.subscribeToSaveResponse(this.productsService.update(products));
    } else {
      this.subscribeToSaveResponse(this.productsService.create(products));
    }
  }

  private createFromForm(): IProducts {
    return {
      ...new Products(),
      id: this.editForm.get(['id'])!.value,
      productName: this.editForm.get(['productName'])!.value,
      categoryID: this.editForm.get(['categoryID'])!.value,
      supplierID: this.editForm.get(['supplierID'])!.value,
      unitPrice: this.editForm.get(['unitPrice'])!.value,
      orderDetailsId: this.editForm.get(['orderDetailsId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProducts>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOrderDetails): any {
    return item.id;
  }
}
