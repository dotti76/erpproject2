import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUserTerritory, UserTerritory } from 'app/shared/model/user-territory.model';
import { UserTerritoryService } from './user-territory.service';
import { UserTerritoryComponent } from './user-territory.component';
import { UserTerritoryDetailComponent } from './user-territory-detail.component';
import { UserTerritoryUpdateComponent } from './user-territory-update.component';

@Injectable({ providedIn: 'root' })
export class UserTerritoryResolve implements Resolve<IUserTerritory> {
  constructor(private service: UserTerritoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserTerritory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((userTerritory: HttpResponse<UserTerritory>) => {
          if (userTerritory.body) {
            return of(userTerritory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UserTerritory());
  }
}

export const userTerritoryRoute: Routes = [
  {
    path: '',
    component: UserTerritoryComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'UserTerritories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: UserTerritoryDetailComponent,
    resolve: {
      userTerritory: UserTerritoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'UserTerritories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: UserTerritoryUpdateComponent,
    resolve: {
      userTerritory: UserTerritoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'UserTerritories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: UserTerritoryUpdateComponent,
    resolve: {
      userTerritory: UserTerritoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'UserTerritories',
    },
    canActivate: [UserRouteAccessService],
  },
];
