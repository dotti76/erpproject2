import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserTerritory } from 'app/shared/model/user-territory.model';
import { UserTerritoryService } from './user-territory.service';

@Component({
  templateUrl: './user-territory-delete-dialog.component.html',
})
export class UserTerritoryDeleteDialogComponent {
  userTerritory?: IUserTerritory;

  constructor(
    protected userTerritoryService: UserTerritoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.userTerritoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('userTerritoryListModification');
      this.activeModal.close();
    });
  }
}
