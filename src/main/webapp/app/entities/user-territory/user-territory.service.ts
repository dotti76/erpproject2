import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserTerritory } from 'app/shared/model/user-territory.model';

type EntityResponseType = HttpResponse<IUserTerritory>;
type EntityArrayResponseType = HttpResponse<IUserTerritory[]>;

@Injectable({ providedIn: 'root' })
export class UserTerritoryService {
  public resourceUrl = SERVER_API_URL + 'api/user-territories';

  constructor(protected http: HttpClient) {}

  create(userTerritory: IUserTerritory): Observable<EntityResponseType> {
    return this.http.post<IUserTerritory>(this.resourceUrl, userTerritory, { observe: 'response' });
  }

  update(userTerritory: IUserTerritory): Observable<EntityResponseType> {
    return this.http.put<IUserTerritory>(this.resourceUrl, userTerritory, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserTerritory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserTerritory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
