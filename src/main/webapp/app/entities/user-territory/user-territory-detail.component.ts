import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserTerritory } from 'app/shared/model/user-territory.model';

@Component({
  selector: 'jhi-user-territory-detail',
  templateUrl: './user-territory-detail.component.html',
})
export class UserTerritoryDetailComponent implements OnInit {
  userTerritory: IUserTerritory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userTerritory }) => (this.userTerritory = userTerritory));
  }

  previousState(): void {
    window.history.back();
  }
}
