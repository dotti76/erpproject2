import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ErpProject2SharedModule } from 'app/shared/shared.module';
import { UserTerritoryComponent } from './user-territory.component';
import { UserTerritoryDetailComponent } from './user-territory-detail.component';
import { UserTerritoryUpdateComponent } from './user-territory-update.component';
import { UserTerritoryDeleteDialogComponent } from './user-territory-delete-dialog.component';
import { userTerritoryRoute } from './user-territory.route';

@NgModule({
  imports: [ErpProject2SharedModule, RouterModule.forChild(userTerritoryRoute)],
  declarations: [UserTerritoryComponent, UserTerritoryDetailComponent, UserTerritoryUpdateComponent, UserTerritoryDeleteDialogComponent],
  entryComponents: [UserTerritoryDeleteDialogComponent],
})
export class ErpProject2UserTerritoryModule {}
