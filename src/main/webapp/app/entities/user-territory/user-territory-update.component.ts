import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserTerritory, UserTerritory } from 'app/shared/model/user-territory.model';
import { UserTerritoryService } from './user-territory.service';

@Component({
  selector: 'jhi-user-territory-update',
  templateUrl: './user-territory-update.component.html',
})
export class UserTerritoryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    territoryId: [],
    userId: [],
  });

  constructor(protected userTerritoryService: UserTerritoryService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userTerritory }) => {
      this.updateForm(userTerritory);
    });
  }

  updateForm(userTerritory: IUserTerritory): void {
    this.editForm.patchValue({
      id: userTerritory.id,
      territoryId: userTerritory.territoryId,
      userId: userTerritory.userId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userTerritory = this.createFromForm();
    if (userTerritory.id !== undefined) {
      this.subscribeToSaveResponse(this.userTerritoryService.update(userTerritory));
    } else {
      this.subscribeToSaveResponse(this.userTerritoryService.create(userTerritory));
    }
  }

  private createFromForm(): IUserTerritory {
    return {
      ...new UserTerritory(),
      id: this.editForm.get(['id'])!.value,
      territoryId: this.editForm.get(['territoryId'])!.value,
      userId: this.editForm.get(['userId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserTerritory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
