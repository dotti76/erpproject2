import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUserTerritory } from 'app/shared/model/user-territory.model';
import { UserTerritoryService } from './user-territory.service';
import { UserTerritoryDeleteDialogComponent } from './user-territory-delete-dialog.component';

@Component({
  selector: 'jhi-user-territory',
  templateUrl: './user-territory.component.html',
})
export class UserTerritoryComponent implements OnInit, OnDestroy {
  userTerritories?: IUserTerritory[];
  eventSubscriber?: Subscription;

  constructor(
    protected userTerritoryService: UserTerritoryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.userTerritoryService.query().subscribe((res: HttpResponse<IUserTerritory[]>) => (this.userTerritories = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInUserTerritories();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUserTerritory): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUserTerritories(): void {
    this.eventSubscriber = this.eventManager.subscribe('userTerritoryListModification', () => this.loadAll());
  }

  delete(userTerritory: IUserTerritory): void {
    const modalRef = this.modalService.open(UserTerritoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.userTerritory = userTerritory;
  }
}
