export interface ICategories {
  id?: number;
  categoryName?: string;
  description?: string;
  productsId?: number;
}

export class Categories implements ICategories {
  constructor(public id?: number, public categoryName?: string, public description?: string, public productsId?: number) {}
}
