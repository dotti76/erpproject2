import { IRegion } from 'app/shared/model/region.model';

export interface ITerritory {
  id?: number;
  regionID?: number;
  name?: string;
  regionIDS?: IRegion[];
}

export class Territory implements ITerritory {
  constructor(public id?: number, public regionID?: number, public name?: string, public regionIDS?: IRegion[]) {}
}
