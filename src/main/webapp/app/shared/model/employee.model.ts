import { Moment } from 'moment';

export interface IEmployee {
  id?: number;
  firstName?: string;
  lastName?: string;
  address?: string;
  phoneNumber?: string;
  hireDate?: Moment;
}

export class Employee implements IEmployee {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public address?: string,
    public phoneNumber?: string,
    public hireDate?: Moment
  ) {}
}
