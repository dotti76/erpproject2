export interface IRegion {
  id?: number;
  regionName?: string;
  userRegionId?: number;
  territoryId?: number;
}

export class Region implements IRegion {
  constructor(public id?: number, public regionName?: string, public userRegionId?: number, public territoryId?: number) {}
}
