import { IOrders } from 'app/shared/model/orders.model';
import { IProducts } from 'app/shared/model/products.model';

export interface IOrderDetails {
  id?: number;
  orderID?: number;
  productID?: number;
  unitPrice?: number;
  quantity?: number;
  orderIDS?: IOrders[];
  productIDS?: IProducts[];
}

export class OrderDetails implements IOrderDetails {
  constructor(
    public id?: number,
    public orderID?: number,
    public productID?: number,
    public unitPrice?: number,
    public quantity?: number,
    public orderIDS?: IOrders[],
    public productIDS?: IProducts[]
  ) {}
}
