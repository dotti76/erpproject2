import { ICategories } from 'app/shared/model/categories.model';
import { ISuppliers } from 'app/shared/model/suppliers.model';

export interface IProducts {
  id?: number;
  productName?: string;
  categoryID?: number;
  supplierID?: number;
  unitPrice?: number;
  orderDetailsId?: number;
  categoryIDS?: ICategories[];
  supplierIDS?: ISuppliers[];
}

export class Products implements IProducts {
  constructor(
    public id?: number,
    public productName?: string,
    public categoryID?: number,
    public supplierID?: number,
    public unitPrice?: number,
    public orderDetailsId?: number,
    public categoryIDS?: ICategories[],
    public supplierIDS?: ISuppliers[]
  ) {}
}
