export interface IShippers {
  id?: number;
  companyName?: string;
  phoneNumber?: string;
  ordersId?: number;
}

export class Shippers implements IShippers {
  constructor(public id?: number, public companyName?: string, public phoneNumber?: string, public ordersId?: number) {}
}
