import { Moment } from 'moment';
import { ICustomers } from 'app/shared/model/customers.model';
import { IShippers } from 'app/shared/model/shippers.model';

export interface IOrders {
  id?: number;
  customerID?: number;
  employeeID?: number;
  orderDate?: Moment;
  shippedDate?: Moment;
  shipVia?: number;
  shipAddress?: string;
  shipCity?: string;
  freight?: string;
  orderDetailsId?: number;
  customerIDS?: ICustomers[];
  shipVias?: IShippers[];
}

export class Orders implements IOrders {
  constructor(
    public id?: number,
    public customerID?: number,
    public employeeID?: number,
    public orderDate?: Moment,
    public shippedDate?: Moment,
    public shipVia?: number,
    public shipAddress?: string,
    public shipCity?: string,
    public freight?: string,
    public orderDetailsId?: number,
    public customerIDS?: ICustomers[],
    public shipVias?: IShippers[]
  ) {}
}
