export interface ISuppliers {
  id?: number;
  companyName?: string;
  productsId?: number;
}

export class Suppliers implements ISuppliers {
  constructor(public id?: number, public companyName?: string, public productsId?: number) {}
}
