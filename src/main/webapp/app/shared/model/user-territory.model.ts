export interface IUserTerritory {
  id?: number;
  territoryId?: number;
  userId?: number;
}

export class UserTerritory implements IUserTerritory {
  constructor(public id?: number, public territoryId?: number, public userId?: number) {}
}
