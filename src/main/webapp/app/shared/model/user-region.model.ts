import { IRegion } from 'app/shared/model/region.model';

export interface IUserRegion {
  id?: number;
  regionID?: number;
  userId?: number;
  regionIDS?: IRegion[];
}

export class UserRegion implements IUserRegion {
  constructor(public id?: number, public regionID?: number, public userId?: number, public regionIDS?: IRegion[]) {}
}
