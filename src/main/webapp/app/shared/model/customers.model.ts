export interface ICustomers {
  id?: number;
  companyName?: string;
  contactName?: string;
  address?: string;
  city?: string;
  region?: string;
  postalCode?: string;
  phone?: string;
  ordersId?: number;
}

export class Customers implements ICustomers {
  constructor(
    public id?: number,
    public companyName?: string,
    public contactName?: string,
    public address?: string,
    public city?: string,
    public region?: string,
    public postalCode?: string,
    public phone?: string,
    public ordersId?: number
  ) {}
}
