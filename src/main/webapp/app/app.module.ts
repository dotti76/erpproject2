import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ErpProject2SharedModule } from 'app/shared/shared.module';
import { ErpProject2CoreModule } from 'app/core/core.module';
import { ErpProject2AppRoutingModule } from './app-routing.module';
import { ErpProject2HomeModule } from './home/home.module';
import { ErpProject2EntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    ErpProject2SharedModule,
    ErpProject2CoreModule,
    ErpProject2HomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ErpProject2EntityModule,
    ErpProject2AppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class ErpProject2AppModule {}
