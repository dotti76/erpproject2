package erp.restClient;

import erp.service.impl.OrdersServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Scanner;
import java.net.URL;

@EnableConfigurationProperties
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Service
public class OpenWeatherClient {

    private final Logger log = LoggerFactory.getLogger(OpenWeatherClient.class);

    @Value("${openweather.baseurl}")
    private String baseurl;

    @Value("${openweather.apikey}")
    private String apikey;

    @Value("${openweather.protocol}")
    private String protocol;

    public String getCurrentWhetherByCity(String city) throws IOException {
        log.debug("OpenWeatherClient: getCurrentWhetherByCity Start");
        String response = null;
        HttpURLConnection connection = (HttpURLConnection) new URL(protocol+"://"+baseurl+"?q="+city+"&appid="+apikey).openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        int responseCode = connection.getResponseCode();
        log.debug("OpenWeatherClient: getCurrentWhetherByCity responseCode:"+responseCode);
        if(responseCode == 200){
            response = "";
            Scanner scanner = new Scanner(connection.getInputStream());
            while(scanner.hasNextLine()){
                response += scanner.nextLine();
                response += "\n";
            }
            scanner.close();
        }
        log.debug("OpenWeatherClient: getCurrentWhetherByCity response:"+response);
        return response;
    }
}
