package erp.web.rest;

import erp.service.ShippersService;
import erp.web.rest.errors.BadRequestAlertException;
import erp.service.dto.ShippersDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link erp.domain.Shippers}.
 */
@RestController
@RequestMapping("/api")
public class ShippersResource {

    private final Logger log = LoggerFactory.getLogger(ShippersResource.class);

    private static final String ENTITY_NAME = "shippers";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShippersService shippersService;

    public ShippersResource(ShippersService shippersService) {
        this.shippersService = shippersService;
    }

    /**
     * {@code POST  /shippers} : Create a new shippers.
     *
     * @param shippersDTO the shippersDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shippersDTO, or with status {@code 400 (Bad Request)} if the shippers has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shippers")
    public ResponseEntity<ShippersDTO> createShippers(@RequestBody ShippersDTO shippersDTO) throws URISyntaxException {
        log.debug("REST request to save Shippers : {}", shippersDTO);
        if (shippersDTO.getId() != null) {
            throw new BadRequestAlertException("A new shippers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShippersDTO result = shippersService.save(shippersDTO);
        return ResponseEntity.created(new URI("/api/shippers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shippers} : Updates an existing shippers.
     *
     * @param shippersDTO the shippersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shippersDTO,
     * or with status {@code 400 (Bad Request)} if the shippersDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shippersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shippers")
    public ResponseEntity<ShippersDTO> updateShippers(@RequestBody ShippersDTO shippersDTO) throws URISyntaxException {
        log.debug("REST request to update Shippers : {}", shippersDTO);
        if (shippersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShippersDTO result = shippersService.save(shippersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, shippersDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shippers} : get all the shippers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shippers in body.
     */
    @GetMapping("/shippers")
    public List<ShippersDTO> getAllShippers() {
        log.debug("REST request to get all Shippers");
        return shippersService.findAll();
    }

    /**
     * {@code GET  /shippers/:id} : get the "id" shippers.
     *
     * @param id the id of the shippersDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shippersDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shippers/{id}")
    public ResponseEntity<ShippersDTO> getShippers(@PathVariable Long id) {
        log.debug("REST request to get Shippers : {}", id);
        Optional<ShippersDTO> shippersDTO = shippersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shippersDTO);
    }

    /**
     * {@code DELETE  /shippers/:id} : delete the "id" shippers.
     *
     * @param id the id of the shippersDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shippers/{id}")
    public ResponseEntity<Void> deleteShippers(@PathVariable Long id) {
        log.debug("REST request to delete Shippers : {}", id);
        shippersService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
