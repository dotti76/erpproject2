package erp.web.rest;

import erp.service.TerritoryService;
import erp.web.rest.errors.BadRequestAlertException;
import erp.service.dto.TerritoryDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.io.IOException;

/**
 * REST controller for managing {@link erp.domain.Territory}.
 */
@RestController
@RequestMapping("/api")
public class TerritoryResource {

    private final Logger log = LoggerFactory.getLogger(TerritoryResource.class);

    private static final String ENTITY_NAME = "territory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TerritoryService territoryService;

    public TerritoryResource(TerritoryService territoryService) {
        this.territoryService = territoryService;
    }

    /**
     * {@code POST  /territories} : Create a new territory.
     *
     * @param territoryDTO the territoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new territoryDTO, or with status {@code 400 (Bad Request)} if the territory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/territories")
    public ResponseEntity<TerritoryDTO> createTerritory(@RequestBody TerritoryDTO territoryDTO) throws URISyntaxException {
        log.debug("REST request to save Territory : {}", territoryDTO);
        if (territoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new territory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TerritoryDTO result = territoryService.save(territoryDTO);
        return ResponseEntity.created(new URI("/api/territories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /territories} : Updates an existing territory.
     *
     * @param territoryDTO the territoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated territoryDTO,
     * or with status {@code 400 (Bad Request)} if the territoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the territoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/territories")
    public ResponseEntity<TerritoryDTO> updateTerritory(@RequestBody TerritoryDTO territoryDTO) throws URISyntaxException {
        log.debug("REST request to update Territory : {}", territoryDTO);
        if (territoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TerritoryDTO result = territoryService.save(territoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, territoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /territories} : get all the territories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of territories in body.
     */
    @GetMapping("/territories")
    public List<TerritoryDTO> getAllTerritories() {
        log.debug("REST request to get all Territories");
        return territoryService.findAll();
    }

    /**
     * {@code GET  /territories/:id} : get the "id" territory.
     *
     * @param id the id of the territoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the territoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/territories/{id}")
    public ResponseEntity<TerritoryDTO> getTerritory(@PathVariable Long id) {
        log.debug("REST request to get Territory : {}", id);
        Optional<TerritoryDTO> territoryDTO = territoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(territoryDTO);
    }

    /**
     * {@code DELETE  /territories/:id} : delete the "id" territory.
     *
     * @param id the id of the territoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/territories/{id}")
    public ResponseEntity<Void> deleteTerritory(@PathVariable Long id) {
        log.debug("REST request to delete Territory : {}", id);
        territoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }


    @GetMapping("/territories/weather/{id}")
    public ResponseEntity<String> getTerritoryWeather(@PathVariable Long id) {
        log.debug("REST request to get getTerritoryWeather : {}", id);
        String result = null;
        try {
            result = territoryService.saveOpenWeather(id);
        } catch (IOException | JSONException ioe) {
            new ResponseStatusException(
                HttpStatus.INTERNAL_SERVER_ERROR, "IO Exception", ioe);
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/territories/favorite")
    public ResponseEntity<String> getTerritoryFavorite() {
        log.debug("REST request to get getTerritoryFavorite");
        String result = territoryService.getTerritoryFavorite();
        return ResponseEntity.ok(result);
    }

}
