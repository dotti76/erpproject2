package erp.web.rest;

import erp.service.UserTerritoryService;
import erp.web.rest.errors.BadRequestAlertException;
import erp.service.dto.UserTerritoryDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link erp.domain.UserTerritory}.
 */
@RestController
@RequestMapping("/api")
public class UserTerritoryResource {

    private final Logger log = LoggerFactory.getLogger(UserTerritoryResource.class);

    private static final String ENTITY_NAME = "userTerritory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserTerritoryService userTerritoryService;

    public UserTerritoryResource(UserTerritoryService userTerritoryService) {
        this.userTerritoryService = userTerritoryService;
    }

    /**
     * {@code POST  /user-territories} : Create a new userTerritory.
     *
     * @param userTerritoryDTO the userTerritoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userTerritoryDTO, or with status {@code 400 (Bad Request)} if the userTerritory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-territories")
    public ResponseEntity<UserTerritoryDTO> createUserTerritory(@RequestBody UserTerritoryDTO userTerritoryDTO) throws URISyntaxException {
        log.debug("REST request to save UserTerritory : {}", userTerritoryDTO);
        if (userTerritoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new userTerritory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserTerritoryDTO result = userTerritoryService.save(userTerritoryDTO);
        return ResponseEntity.created(new URI("/api/user-territories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-territories} : Updates an existing userTerritory.
     *
     * @param userTerritoryDTO the userTerritoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userTerritoryDTO,
     * or with status {@code 400 (Bad Request)} if the userTerritoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userTerritoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-territories")
    public ResponseEntity<UserTerritoryDTO> updateUserTerritory(@RequestBody UserTerritoryDTO userTerritoryDTO) throws URISyntaxException {
        log.debug("REST request to update UserTerritory : {}", userTerritoryDTO);
        if (userTerritoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserTerritoryDTO result = userTerritoryService.save(userTerritoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userTerritoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-territories} : get all the userTerritories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userTerritories in body.
     */
    @GetMapping("/user-territories")
    public List<UserTerritoryDTO> getAllUserTerritories() {
        log.debug("REST request to get all UserTerritories");
        return userTerritoryService.findAll();
    }

    /**
     * {@code GET  /user-territories/:id} : get the "id" userTerritory.
     *
     * @param id the id of the userTerritoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userTerritoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-territories/{id}")
    public ResponseEntity<UserTerritoryDTO> getUserTerritory(@PathVariable Long id) {
        log.debug("REST request to get UserTerritory : {}", id);
        Optional<UserTerritoryDTO> userTerritoryDTO = userTerritoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userTerritoryDTO);
    }

    /**
     * {@code DELETE  /user-territories/:id} : delete the "id" userTerritory.
     *
     * @param id the id of the userTerritoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-territories/{id}")
    public ResponseEntity<Void> deleteUserTerritory(@PathVariable Long id) {
        log.debug("REST request to delete UserTerritory : {}", id);
        userTerritoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
