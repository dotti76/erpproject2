package erp.web.rest;

import erp.service.UserRegionService;
import erp.web.rest.errors.BadRequestAlertException;
import erp.service.dto.UserRegionDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link erp.domain.UserRegion}.
 */
@RestController
@RequestMapping("/api")
public class UserRegionResource {

    private final Logger log = LoggerFactory.getLogger(UserRegionResource.class);

    private static final String ENTITY_NAME = "userRegion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserRegionService userRegionService;

    public UserRegionResource(UserRegionService userRegionService) {
        this.userRegionService = userRegionService;
    }

    /**
     * {@code POST  /user-regions} : Create a new userRegion.
     *
     * @param userRegionDTO the userRegionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userRegionDTO, or with status {@code 400 (Bad Request)} if the userRegion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-regions")
    public ResponseEntity<UserRegionDTO> createUserRegion(@RequestBody UserRegionDTO userRegionDTO) throws URISyntaxException {
        log.debug("REST request to save UserRegion : {}", userRegionDTO);
        if (userRegionDTO.getId() != null) {
            throw new BadRequestAlertException("A new userRegion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserRegionDTO result = userRegionService.save(userRegionDTO);
        return ResponseEntity.created(new URI("/api/user-regions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-regions} : Updates an existing userRegion.
     *
     * @param userRegionDTO the userRegionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userRegionDTO,
     * or with status {@code 400 (Bad Request)} if the userRegionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userRegionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-regions")
    public ResponseEntity<UserRegionDTO> updateUserRegion(@RequestBody UserRegionDTO userRegionDTO) throws URISyntaxException {
        log.debug("REST request to update UserRegion : {}", userRegionDTO);
        if (userRegionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserRegionDTO result = userRegionService.save(userRegionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userRegionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-regions} : get all the userRegions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userRegions in body.
     */
    @GetMapping("/user-regions")
    public List<UserRegionDTO> getAllUserRegions() {
        log.debug("REST request to get all UserRegions");
        return userRegionService.findAll();
    }

    /**
     * {@code GET  /user-regions/:id} : get the "id" userRegion.
     *
     * @param id the id of the userRegionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userRegionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-regions/{id}")
    public ResponseEntity<UserRegionDTO> getUserRegion(@PathVariable Long id) {
        log.debug("REST request to get UserRegion : {}", id);
        Optional<UserRegionDTO> userRegionDTO = userRegionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userRegionDTO);
    }

    /**
     * {@code DELETE  /user-regions/:id} : delete the "id" userRegion.
     *
     * @param id the id of the userRegionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-regions/{id}")
    public ResponseEntity<Void> deleteUserRegion(@PathVariable Long id) {
        log.debug("REST request to delete UserRegion : {}", id);
        userRegionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
