/**
 * View Models used by Spring MVC REST controllers.
 */
package erp.web.rest.vm;
