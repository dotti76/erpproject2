package erp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Task entity.\n@author The JHipster team.
 */
@Entity
@Table(name = "products")
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "category_id")
    private Long categoryID;

    @Column(name = "supplier_id")
    private Long supplierID;

    @Column(name = "unit_price")
    private Long unitPrice;

    @ManyToOne
    @JsonIgnoreProperties(value = "productIDS", allowSetters = true)
    private OrderDetails orderDetails;

    @OneToMany(mappedBy = "products")
    private Set<Categories> categoryIDS = new HashSet<>();

    @OneToMany(mappedBy = "products")
    private Set<Suppliers> supplierIDS = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public Products productName(String productName) {
        this.productName = productName;
        return this;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getCategoryID() {
        return categoryID;
    }

    public Products categoryID(Long categoryID) {
        this.categoryID = categoryID;
        return this;
    }

    public void setCategoryID(Long categoryID) {
        this.categoryID = categoryID;
    }

    public Long getSupplierID() {
        return supplierID;
    }

    public Products supplierID(Long supplierID) {
        this.supplierID = supplierID;
        return this;
    }

    public void setSupplierID(Long supplierID) {
        this.supplierID = supplierID;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public Products unitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public OrderDetails getOrderDetails() {
        return orderDetails;
    }

    public Products orderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
        return this;
    }

    public void setOrderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Set<Categories> getCategoryIDS() {
        return categoryIDS;
    }

    public Products categoryIDS(Set<Categories> categories) {
        this.categoryIDS = categories;
        return this;
    }

    public Products addCategoryID(Categories categories) {
        this.categoryIDS.add(categories);
        categories.setProducts(this);
        return this;
    }

    public Products removeCategoryID(Categories categories) {
        this.categoryIDS.remove(categories);
        categories.setProducts(null);
        return this;
    }

    public void setCategoryIDS(Set<Categories> categories) {
        this.categoryIDS = categories;
    }

    public Set<Suppliers> getSupplierIDS() {
        return supplierIDS;
    }

    public Products supplierIDS(Set<Suppliers> suppliers) {
        this.supplierIDS = suppliers;
        return this;
    }

    public Products addSupplierID(Suppliers suppliers) {
        this.supplierIDS.add(suppliers);
        suppliers.setProducts(this);
        return this;
    }

    public Products removeSupplierID(Suppliers suppliers) {
        this.supplierIDS.remove(suppliers);
        suppliers.setProducts(null);
        return this;
    }

    public void setSupplierIDS(Set<Suppliers> suppliers) {
        this.supplierIDS = suppliers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Products)) {
            return false;
        }
        return id != null && id.equals(((Products) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Products{" +
            "id=" + getId() +
            ", productName='" + getProductName() + "'" +
            ", categoryID=" + getCategoryID() +
            ", supplierID=" + getSupplierID() +
            ", unitPrice=" + getUnitPrice() +
            "}";
    }
}
