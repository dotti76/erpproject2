package erp.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * not an ignored comment
 */
@Entity
@Table(name = "territory")
public class Territory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "region_id")
    private Long regionID;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "territory")
    private Set<Region> regionIDS = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRegionID() {
        return regionID;
    }

    public Territory regionID(Long regionID) {
        this.regionID = regionID;
        return this;
    }

    public void setRegionID(Long regionID) {
        this.regionID = regionID;
    }

    public String getName() {
        return name;
    }

    public Territory name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Region> getRegionIDS() {
        return regionIDS;
    }

    public Territory regionIDS(Set<Region> regions) {
        this.regionIDS = regions;
        return this;
    }

    public Territory addRegionID(Region region) {
        this.regionIDS.add(region);
        region.setTerritory(this);
        return this;
    }

    public Territory removeRegionID(Region region) {
        this.regionIDS.remove(region);
        region.setTerritory(null);
        return this;
    }

    public void setRegionIDS(Set<Region> regions) {
        this.regionIDS = regions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Territory)) {
            return false;
        }
        return id != null && id.equals(((Territory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Territory{" +
            "id=" + getId() +
            ", regionID=" + getRegionID() +
            ", name='" + getName() + "'" +
            "}";
    }
}
