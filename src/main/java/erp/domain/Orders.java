package erp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Orders.
 */
@Entity
@Table(name = "orders")
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "customer_id")
    private Long customerID;

    @Column(name = "employee_id")
    private Long employeeID;

    @Column(name = "order_date")
    private Instant orderDate;

    @Column(name = "shipped_date")
    private Instant shippedDate;

    @Column(name = "ship_via")
    private Long shipVia;

    @Column(name = "ship_address")
    private String shipAddress;

    @Column(name = "ship_city")
    private String shipCity;

    @Column(name = "freight")
    private String freight;

    @ManyToOne
    @JsonIgnoreProperties(value = "orderIDS", allowSetters = true)
    private OrderDetails orderDetails;

    @OneToMany(mappedBy = "orders")
    private Set<Customers> customerIDS = new HashSet<>();

    @OneToMany(mappedBy = "orders")
    private Set<Shippers> shipVias = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public Orders customerID(Long customerID) {
        this.customerID = customerID;
        return this;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getEmployeeID() {
        return employeeID;
    }

    public Orders employeeID(Long employeeID) {
        this.employeeID = employeeID;
        return this;
    }

    public void setEmployeeID(Long employeeID) {
        this.employeeID = employeeID;
    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public Orders orderDate(Instant orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }

    public Instant getShippedDate() {
        return shippedDate;
    }

    public Orders shippedDate(Instant shippedDate) {
        this.shippedDate = shippedDate;
        return this;
    }

    public void setShippedDate(Instant shippedDate) {
        this.shippedDate = shippedDate;
    }

    public Long getShipVia() {
        return shipVia;
    }

    public Orders shipVia(Long shipVia) {
        this.shipVia = shipVia;
        return this;
    }

    public void setShipVia(Long shipVia) {
        this.shipVia = shipVia;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public Orders shipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
        return this;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getShipCity() {
        return shipCity;
    }

    public Orders shipCity(String shipCity) {
        this.shipCity = shipCity;
        return this;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getFreight() {
        return freight;
    }

    public Orders freight(String freight) {
        this.freight = freight;
        return this;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public OrderDetails getOrderDetails() {
        return orderDetails;
    }

    public Orders orderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
        return this;
    }

    public void setOrderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Set<Customers> getCustomerIDS() {
        return customerIDS;
    }

    public Orders customerIDS(Set<Customers> customers) {
        this.customerIDS = customers;
        return this;
    }

    public Orders addCustomerID(Customers customers) {
        this.customerIDS.add(customers);
        customers.setOrders(this);
        return this;
    }

    public Orders removeCustomerID(Customers customers) {
        this.customerIDS.remove(customers);
        customers.setOrders(null);
        return this;
    }

    public void setCustomerIDS(Set<Customers> customers) {
        this.customerIDS = customers;
    }

    public Set<Shippers> getShipVias() {
        return shipVias;
    }

    public Orders shipVias(Set<Shippers> shippers) {
        this.shipVias = shippers;
        return this;
    }

    public Orders addShipVia(Shippers shippers) {
        this.shipVias.add(shippers);
        shippers.setOrders(this);
        return this;
    }

    public Orders removeShipVia(Shippers shippers) {
        this.shipVias.remove(shippers);
        shippers.setOrders(null);
        return this;
    }

    public void setShipVias(Set<Shippers> shippers) {
        this.shipVias = shippers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Orders)) {
            return false;
        }
        return id != null && id.equals(((Orders) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Orders{" +
            "id=" + getId() +
            ", customerID=" + getCustomerID() +
            ", employeeID=" + getEmployeeID() +
            ", orderDate='" + getOrderDate() + "'" +
            ", shippedDate='" + getShippedDate() + "'" +
            ", shipVia=" + getShipVia() +
            ", shipAddress='" + getShipAddress() + "'" +
            ", shipCity='" + getShipCity() + "'" +
            ", freight='" + getFreight() + "'" +
            "}";
    }
}
