package erp.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A UserRegion.
 */
@Entity
@Table(name = "user_region")
public class UserRegion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "region_id")
    private Long regionID;

    @Column(name = "user_id")
    private Long userId;

    @OneToMany(mappedBy = "userRegion")
    private Set<Region> regionIDS = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRegionID() {
        return regionID;
    }

    public UserRegion regionID(Long regionID) {
        this.regionID = regionID;
        return this;
    }

    public void setRegionID(Long regionID) {
        this.regionID = regionID;
    }

    public Long getUserId() {
        return userId;
    }

    public UserRegion userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<Region> getRegionIDS() {
        return regionIDS;
    }

    public UserRegion regionIDS(Set<Region> regions) {
        this.regionIDS = regions;
        return this;
    }

    public UserRegion addRegionID(Region region) {
        this.regionIDS.add(region);
        region.setUserRegion(this);
        return this;
    }

    public UserRegion removeRegionID(Region region) {
        this.regionIDS.remove(region);
        region.setUserRegion(null);
        return this;
    }

    public void setRegionIDS(Set<Region> regions) {
        this.regionIDS = regions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserRegion)) {
            return false;
        }
        return id != null && id.equals(((UserRegion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserRegion{" +
            "id=" + getId() +
            ", regionID=" + getRegionID() +
            ", userId=" + getUserId() +
            "}";
    }
}
