package erp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.Instant;

/**
 * A OrdersJSON.
 */
@Document(collection = "orders_json")
public class OrdersJSON implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("customer_id")
    private Long customerID;

    @Field("employee_id")
    private Long employeeID;

    @Field("order_date")
    private Instant orderDate;

    @Field("shipped_date")
    private Instant shippedDate;

    @Field("ship_via")
    private Long shipVia;

    @Field("ship_address")
    private String shipAddress;

    @Field("ship_city")
    private String shipCity;

    @Field("freight")
    private String freight;

    @Field("product_id")
    private Long productID;

    @Field("unit_price")
    private Long unitPrice;

    @Field("quantity")
    private Long quantity;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public OrdersJSON customerID(Long customerID) {
        this.customerID = customerID;
        return this;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getEmployeeID() {
        return employeeID;
    }

    public OrdersJSON employeeID(Long employeeID) {
        this.employeeID = employeeID;
        return this;
    }

    public void setEmployeeID(Long employeeID) {
        this.employeeID = employeeID;
    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public OrdersJSON orderDate(Instant orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }

    public Instant getShippedDate() {
        return shippedDate;
    }

    public OrdersJSON shippedDate(Instant shippedDate) {
        this.shippedDate = shippedDate;
        return this;
    }

    public void setShippedDate(Instant shippedDate) {
        this.shippedDate = shippedDate;
    }

    public Long getShipVia() {
        return shipVia;
    }

    public OrdersJSON shipVia(Long shipVia) {
        this.shipVia = shipVia;
        return this;
    }

    public void setShipVia(Long shipVia) {
        this.shipVia = shipVia;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public OrdersJSON shipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
        return this;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getShipCity() {
        return shipCity;
    }

    public OrdersJSON shipCity(String shipCity) {
        this.shipCity = shipCity;
        return this;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getFreight() {
        return freight;
    }

    public OrdersJSON freight(String freight) {
        this.freight = freight;
        return this;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public Long getProductID() {
        return productID;
    }

    public OrdersJSON productID(Long productID) {
        this.productID = productID;
        return this;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public OrdersJSON unitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Long getQuantity() {
        return quantity;
    }

    public OrdersJSON quantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrdersJSON)) {
            return false;
        }
        return id != null && id.equals(((OrdersJSON) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdersJSON{" +
            "id=" + getId() +
            ", customerID=" + getCustomerID() +
            ", employeeID=" + getEmployeeID() +
            ", orderDate='" + getOrderDate() + "'" +
            ", shippedDate='" + getShippedDate() + "'" +
            ", shipVia=" + getShipVia() +
            ", shipAddress='" + getShipAddress() + "'" +
            ", shipCity='" + getShipCity() + "'" +
            ", freight='" + getFreight() + "'" +
            ", productID=" + getProductID() +
            ", unitPrice=" + getUnitPrice() +
            ", quantity=" + getQuantity() +
            "}";
    }
}
