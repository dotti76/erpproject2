package erp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.io.Serializable;

/**
 * A OrdersJSON.
 */
@Document(collection = "weather_json")
public class WeatherJSON implements Serializable{

    @Id
    private String id;

    @Field("territory_id")
    private String territoryId;

    @Field("territory_name")
    private String territoryName;

    @Field("dt")
    private Long data;

    @Field("temp_min")
    private Double tempMin;

    @Field("temp_max")
    private Double tempMax;

    @Field("response")
    private String response;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTerritoryId() {
        return territoryId;
    }

    public void setTerritoryId(String territoryId) {
        this.territoryId = territoryId;
    }

    public String getTerritoryName() {
        return territoryName;
    }

    public void setTerritoryName(String territoryName) {
        this.territoryName = territoryName;
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }

    public Double getTempMin() {
        return tempMin;
    }

    public void setTempMin(Double tempMin) {
        this.tempMin = tempMin;
    }

    public Double getTempMax() {
        return tempMax;
    }

    public void setTempMax(Double tempMax) {
        this.tempMax = tempMax;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public WeatherJSON(String id, String territoryId, String territoryName, Long data, Double tempMin, Double tempMax, String response) {
        this.id = id;
        this.territoryId = territoryId;
        this.territoryName = territoryName;
        this.data = data;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
        this.response = response;
    }


    @java.lang.Override
    public java.lang.String toString() {
        return "WeatherJSON{" +
            "id='" + id + '\'' +
            ", territoryId='" + territoryId + '\'' +
            ", territoryName='" + territoryName + '\'' +
            ", data='" + data + '\'' +
            ", tempMin=" + tempMin +
            ", tempMax=" + tempMax +
            ", response='" + response + '\'' +
            '}';
    }
}
