package erp.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A OrderDetails.
 */
@Entity
@Table(name = "order_details")
public class OrderDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "order_id")
    private Long orderID;

    @Column(name = "product_id")
    private Long productID;

    @Column(name = "unit_price")
    private Long unitPrice;

    @Column(name = "quantity")
    private Long quantity;

    @OneToMany(mappedBy = "orderDetails")
    private Set<Orders> orderIDS = new HashSet<>();

    @OneToMany(mappedBy = "orderDetails")
    private Set<Products> productIDS = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderID() {
        return orderID;
    }

    public OrderDetails orderID(Long orderID) {
        this.orderID = orderID;
        return this;
    }

    public void setOrderID(Long orderID) {
        this.orderID = orderID;
    }

    public Long getProductID() {
        return productID;
    }

    public OrderDetails productID(Long productID) {
        this.productID = productID;
        return this;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public OrderDetails unitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Long getQuantity() {
        return quantity;
    }

    public OrderDetails quantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Set<Orders> getOrderIDS() {
        return orderIDS;
    }

    public OrderDetails orderIDS(Set<Orders> orders) {
        this.orderIDS = orders;
        return this;
    }

    public OrderDetails addOrderID(Orders orders) {
        this.orderIDS.add(orders);
        orders.setOrderDetails(this);
        return this;
    }

    public OrderDetails removeOrderID(Orders orders) {
        this.orderIDS.remove(orders);
        orders.setOrderDetails(null);
        return this;
    }

    public void setOrderIDS(Set<Orders> orders) {
        this.orderIDS = orders;
    }

    public Set<Products> getProductIDS() {
        return productIDS;
    }

    public OrderDetails productIDS(Set<Products> products) {
        this.productIDS = products;
        return this;
    }

    public OrderDetails addProductID(Products products) {
        this.productIDS.add(products);
        products.setOrderDetails(this);
        return this;
    }

    public OrderDetails removeProductID(Products products) {
        this.productIDS.remove(products);
        products.setOrderDetails(null);
        return this;
    }

    public void setProductIDS(Set<Products> products) {
        this.productIDS = products;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderDetails)) {
            return false;
        }
        return id != null && id.equals(((OrderDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderDetails{" +
            "id=" + getId() +
            ", orderID=" + getOrderID() +
            ", productID=" + getProductID() +
            ", unitPrice=" + getUnitPrice() +
            ", quantity=" + getQuantity() +
            "}";
    }
}
