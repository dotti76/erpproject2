package erp.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A UserTerritory.
 */
@Entity
@Table(name = "user_territory")
public class UserTerritory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "territory_id")
    private Long territoryId;

    @Column(name = "user_id")
    private Long userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTerritoryId() {
        return territoryId;
    }

    public UserTerritory territoryId(Long territoryId) {
        this.territoryId = territoryId;
        return this;
    }

    public void setTerritoryId(Long territoryId) {
        this.territoryId = territoryId;
    }

    public Long getUserId() {
        return userId;
    }

    public UserTerritory userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserTerritory)) {
            return false;
        }
        return id != null && id.equals(((UserTerritory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserTerritory{" +
            "id=" + getId() +
            ", territoryId=" + getTerritoryId() +
            ", userId=" + getUserId() +
            "}";
    }
}
