package erp.service.impl;

import erp.domain.User;
import erp.domain.WeatherJSON;
import erp.repository.UserRepository;
import erp.repository.WeatherJSONRepository;
import erp.restClient.OpenWeatherClient;
import erp.service.TerritoryService;
import erp.domain.Territory;
import erp.repository.TerritoryRepository;
import erp.service.dto.TerritoryDTO;
import erp.service.mapper.TerritoryMapper;
import org.mapstruct.ap.internal.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.io.IOException;
import java.time.Instant;
import java.util.Map;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.sql.Timestamp;


/**
 * Service Implementation for managing {@link Territory}.
 */
@Service
@Transactional
public class TerritoryServiceImpl implements TerritoryService {

    private final Logger log = LoggerFactory.getLogger(TerritoryServiceImpl.class);

    private final TerritoryRepository territoryRepository;

    private final WeatherJSONRepository weatherJSONRepository;

    private final TerritoryMapper territoryMapper;

    private final OpenWeatherClient openWeatherClient;

    private final UserRepository userRepository;

    public TerritoryServiceImpl(TerritoryRepository territoryRepository, TerritoryMapper territoryMapper,OpenWeatherClient openWeatherClient,WeatherJSONRepository weatherJSONRepository,UserRepository userRepository) {
        this.territoryRepository = territoryRepository;
        this.territoryMapper = territoryMapper;
        this.openWeatherClient = openWeatherClient;
        this.weatherJSONRepository = weatherJSONRepository;
        this.userRepository = userRepository;
    }

    @Override
    public TerritoryDTO save(TerritoryDTO territoryDTO) {
        log.debug("Request to save Territory : {}", territoryDTO);
        Territory territory = territoryMapper.toEntity(territoryDTO);
        territory = territoryRepository.save(territory);
        return territoryMapper.toDto(territory);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TerritoryDTO> findAll() {
        log.debug("Request to get all Territories");
        return territoryRepository.findAll().stream()
            .map(territoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<TerritoryDTO> findOne(Long id) {
        log.debug("Request to get Territory : {}", id);
        return territoryRepository.findById(id)
            .map(territoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Territory : {}", id);
        territoryRepository.deleteById(id);
    }

    public String openWeatherByTerritory(Long id) throws IOException {
        String jsonResponseWeather = null;
        log.debug("Request openWeatherByTerritory Territory : {}", id);
        Optional<Territory> optional = territoryRepository.findById(id);
        jsonResponseWeather = openWeatherClient.getCurrentWhetherByCity(optional.get().getName());
        log.debug("Response openWeatherByTerritory jsonResponseWeather:", jsonResponseWeather);
        return jsonResponseWeather;
    }

    public String saveOpenWeather(Long id) throws IOException, JSONException {
        String result = "OK";
        log.debug("Request saveOpenWeather Territory : {}", id);
        String jsonResponseWeather = openWeatherByTerritory(id);
        String territoryName = null;
        Double tempMin = null;
        Double tempMax = null;
        String weather = null;
        Long data = null;
        if (!Strings.isEmpty(jsonResponseWeather)) {
            JSONObject responseWeatherJSONObject = new JSONObject(jsonResponseWeather);
            tempMin = responseWeatherJSONObject.getJSONObject("main").getDouble("temp_min");
            tempMax = responseWeatherJSONObject.getJSONObject("main").getDouble("temp_max");
            territoryName = responseWeatherJSONObject.getString("name");
            data = responseWeatherJSONObject.getLong("dt");
            log.debug("Request saveOpenWeather tempMin : ", tempMin);
        }
        WeatherJSON weatherJSON = new WeatherJSON(null,""+id,territoryName,data,tempMin,tempMax,jsonResponseWeather);
        weatherJSONRepository.save(weatherJSON);
        return result;
    }

    public String getTerritoryFavorite() {
        log.debug("getTerritoryFavorite");
        String result = "";
//        Map<String,Object> userSaleResult = userRepository.userSalesByOrder(Instant.ofEpochMilli(655034247),Instant.now());
        List<User>  userSaleResult = userRepository.userSalesByOrder();
        log.debug("userSaleResult:"+userSaleResult);
        return result;
    }
}
