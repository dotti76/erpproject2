package erp.service.impl;

import erp.service.UserTerritoryService;
import erp.domain.UserTerritory;
import erp.repository.UserTerritoryRepository;
import erp.service.dto.UserTerritoryDTO;
import erp.service.mapper.UserTerritoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link UserTerritory}.
 */
@Service
@Transactional
public class UserTerritoryServiceImpl implements UserTerritoryService {

    private final Logger log = LoggerFactory.getLogger(UserTerritoryServiceImpl.class);

    private final UserTerritoryRepository userTerritoryRepository;

    private final UserTerritoryMapper userTerritoryMapper;

    public UserTerritoryServiceImpl(UserTerritoryRepository userTerritoryRepository, UserTerritoryMapper userTerritoryMapper) {
        this.userTerritoryRepository = userTerritoryRepository;
        this.userTerritoryMapper = userTerritoryMapper;
    }

    @Override
    public UserTerritoryDTO save(UserTerritoryDTO userTerritoryDTO) {
        log.debug("Request to save UserTerritory : {}", userTerritoryDTO);
        UserTerritory userTerritory = userTerritoryMapper.toEntity(userTerritoryDTO);
        userTerritory = userTerritoryRepository.save(userTerritory);
        return userTerritoryMapper.toDto(userTerritory);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserTerritoryDTO> findAll() {
        log.debug("Request to get all UserTerritories");
        return userTerritoryRepository.findAll().stream()
            .map(userTerritoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<UserTerritoryDTO> findOne(Long id) {
        log.debug("Request to get UserTerritory : {}", id);
        return userTerritoryRepository.findById(id)
            .map(userTerritoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserTerritory : {}", id);
        userTerritoryRepository.deleteById(id);
    }
}
