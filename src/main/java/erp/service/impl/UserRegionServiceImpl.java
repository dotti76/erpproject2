package erp.service.impl;

import erp.service.UserRegionService;
import erp.domain.UserRegion;
import erp.repository.UserRegionRepository;
import erp.service.dto.UserRegionDTO;
import erp.service.mapper.UserRegionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link UserRegion}.
 */
@Service
@Transactional
public class UserRegionServiceImpl implements UserRegionService {

    private final Logger log = LoggerFactory.getLogger(UserRegionServiceImpl.class);

    private final UserRegionRepository userRegionRepository;

    private final UserRegionMapper userRegionMapper;

    public UserRegionServiceImpl(UserRegionRepository userRegionRepository, UserRegionMapper userRegionMapper) {
        this.userRegionRepository = userRegionRepository;
        this.userRegionMapper = userRegionMapper;
    }

    @Override
    public UserRegionDTO save(UserRegionDTO userRegionDTO) {
        log.debug("Request to save UserRegion : {}", userRegionDTO);
        UserRegion userRegion = userRegionMapper.toEntity(userRegionDTO);
        userRegion = userRegionRepository.save(userRegion);
        return userRegionMapper.toDto(userRegion);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserRegionDTO> findAll() {
        log.debug("Request to get all UserRegions");
        return userRegionRepository.findAll().stream()
            .map(userRegionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<UserRegionDTO> findOne(Long id) {
        log.debug("Request to get UserRegion : {}", id);
        return userRegionRepository.findById(id)
            .map(userRegionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserRegion : {}", id);
        userRegionRepository.deleteById(id);
    }
}
