package erp.service.impl;

import erp.service.ShippersService;
import erp.domain.Shippers;
import erp.repository.ShippersRepository;
import erp.service.dto.ShippersDTO;
import erp.service.mapper.ShippersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Shippers}.
 */
@Service
@Transactional
public class ShippersServiceImpl implements ShippersService {

    private final Logger log = LoggerFactory.getLogger(ShippersServiceImpl.class);

    private final ShippersRepository shippersRepository;

    private final ShippersMapper shippersMapper;

    public ShippersServiceImpl(ShippersRepository shippersRepository, ShippersMapper shippersMapper) {
        this.shippersRepository = shippersRepository;
        this.shippersMapper = shippersMapper;
    }

    @Override
    public ShippersDTO save(ShippersDTO shippersDTO) {
        log.debug("Request to save Shippers : {}", shippersDTO);
        Shippers shippers = shippersMapper.toEntity(shippersDTO);
        shippers = shippersRepository.save(shippers);
        return shippersMapper.toDto(shippers);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShippersDTO> findAll() {
        log.debug("Request to get all Shippers");
        return shippersRepository.findAll().stream()
            .map(shippersMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ShippersDTO> findOne(Long id) {
        log.debug("Request to get Shippers : {}", id);
        return shippersRepository.findById(id)
            .map(shippersMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Shippers : {}", id);
        shippersRepository.deleteById(id);
    }
}
