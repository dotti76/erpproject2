package erp.service.impl;

import erp.domain.OrderDetails;
import erp.domain.OrdersJSON;
import erp.repository.OrderDetailsRepository;
import erp.repository.OrdersJSONRepository;
import erp.service.OrdersService;
import erp.domain.Orders;
import erp.repository.OrdersRepository;
import erp.service.dto.OrderDetailsDTO;
import erp.service.dto.OrdersDTO;
import erp.service.dto.OrdersCompleteDTO;
import erp.service.dto.OrdersJSONDTO;
import erp.service.mapper.OrderDetailsMapper;
import erp.service.mapper.OrdersJSONMapper;
import erp.service.mapper.OrdersMapper;
import org.mapstruct.ap.internal.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.ArrayList;

/**
 * Service Implementation for managing {@link Orders}.
 */
@Service
@Transactional
public class OrdersServiceImpl implements OrdersService {

    private final Logger log = LoggerFactory.getLogger(OrdersServiceImpl.class);

    private final OrdersRepository ordersRepository;

    private final OrdersMapper ordersMapper;

    private final OrderDetailsRepository orderDetailsRepository;

    private final OrderDetailsMapper orderDetailsMapper;

    private final OrdersJSONRepository ordersJSONRepository;

    private final OrdersJSONMapper ordersJSONMapper;

    public OrdersServiceImpl(OrdersRepository ordersRepository, OrdersMapper ordersMapper, OrderDetailsRepository orderDetailsRepository, OrderDetailsMapper orderDetailsMapper,OrdersJSONRepository orderJSONRepository,OrdersJSONMapper ordersJSONMapper) {
        this.ordersRepository = ordersRepository;
        this.ordersMapper = ordersMapper;
        this.orderDetailsRepository = orderDetailsRepository;
        this.orderDetailsMapper = orderDetailsMapper;
        this.ordersJSONRepository = orderJSONRepository;
        this.ordersJSONMapper = ordersJSONMapper;
    }

    @Override
    public OrdersDTO save(OrdersDTO ordersDTO) {
        log.debug("Request to save Orders : {}", ordersDTO);
        Orders orders = ordersMapper.toEntity(ordersDTO);
        orders = ordersRepository.save(orders);
        return ordersMapper.toDto(orders);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrdersDTO> findAll() {
        log.debug("Request to get all Orders");
        return ordersRepository.findAll().stream()
            .map(ordersMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<OrdersDTO> findOne(Long id) {
        log.debug("Request to get Orders : {}", id);
        return ordersRepository.findById(id)
            .map(ordersMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Orders : {}", id);
        ordersRepository.deleteById(id);
    }

    @Override
    public OrdersDTO insertComplete(OrdersCompleteDTO ordersCompleteDTO) {
        log.debug("Request insertComplete Orders : {}", ordersCompleteDTO);
        OrdersDTO orderDTO = new OrdersDTO(null, ordersCompleteDTO.getCustomerID(), ordersCompleteDTO.getEmployeeID(), ordersCompleteDTO.getOrderDate(),
            ordersCompleteDTO.getShippedDate(), ordersCompleteDTO.getShipVia(), ordersCompleteDTO.getShipAddress(), ordersCompleteDTO.getShipCity(),
            ordersCompleteDTO.getFreight(), null);
        // inserisco ordine
        Orders orders = ordersMapper.toEntity(orderDTO);
        orders = ordersRepository.save(orders);
        OrdersDTO orderDTOResult = ordersMapper.toDto(orders);
        // inserisco dettaglio ordine
        OrderDetailsDTO orderDetailDTO = new OrderDetailsDTO(null, orderDTOResult.getId(), ordersCompleteDTO.getProductID(), ordersCompleteDTO.getUnitPrice(), ordersCompleteDTO.getQuantity());
        OrderDetails orderDetails = orderDetailsMapper.toEntity(orderDetailDTO);
        orderDetails = orderDetailsRepository.save(orderDetails);
        return orderDTOResult;
    }

    @Override
    public String copyOrderToMongo() {
        log.debug("copyOrderToMongo");
        String message = "OK";
        // leggo tutti gli ordini
        List<OrdersDTO> listOrders = findAll();
        List<OrdersJSONDTO> listOrdersJSONDTO = new  ArrayList<OrdersJSONDTO>();
        for (OrdersDTO orderDTO:listOrders) {
            // prendo dettaglio ordine
            List<OrderDetails> listOrderDetails = orderDetailsRepository.findByOrderID(orderDTO.getId());
            OrderDetailsDTO detailDTO = new OrderDetailsDTO();
            if (listOrderDetails != null && listOrderDetails.size() > 0) {
                OrderDetails orderDetail = listOrderDetails.get(0);
                log.debug("prendo dettaglio ordine orderDetail:" + orderDetail);
                detailDTO.setProductID(orderDetail.getProductID());
                detailDTO.setQuantity(orderDetail.getQuantity());
                detailDTO.setUnitPrice(orderDetail.getUnitPrice());
            }
            OrdersJSONDTO orderJSONDTO = new OrdersJSONDTO("" + orderDTO.getId(), orderDTO.getCustomerID(), orderDTO.getEmployeeID(), orderDTO.getOrderDate(), orderDTO.getShippedDate(), orderDTO.getShipVia(), orderDTO.getShipAddress(),
                orderDTO.getShipCity(), orderDTO.getFreight(), detailDTO.getProductID(), detailDTO.getUnitPrice(), detailDTO.getQuantity());
            listOrdersJSONDTO.add(orderJSONDTO);
        }
        log.debug("salvo listOrdersJSONDTO:"+listOrdersJSONDTO);
        List<OrdersJSON> listOrderJSON =  ordersJSONMapper.toEntity(listOrdersJSONDTO);
        List<OrdersJSON> listordersJSONResult = listOrderJSON.stream().map(ordersJSONRepository::save).collect(Collectors.toList());
        log.debug("salvo listordersJSONResult:"+listordersJSONResult);
        return message;
    }



    @Override
    public List<OrdersJSONDTO> findAllFromMongo(OrdersJSONDTO searchDTO) {
        log.debug("Request to get all findAllFromMongo");
        if (searchDTO.getId()==null) {
            // search all orders from mongo
            log.debug("Request to get all");
            return ordersJSONRepository.findAll().stream()
                .map(ordersJSONMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
        } else {
            // search order by id from mongo
            log.debug("Request to get by id:"+searchDTO.getId());
            return ordersJSONRepository.findById(""+searchDTO.getId()).stream()
                .map(ordersJSONMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
        }
    }

}
