package erp.service;

import erp.service.dto.TerritoryDTO;
import org.springframework.boot.configurationprocessor.json.JSONException;

import java.io.IOException;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link erp.domain.Territory}.
 */
public interface TerritoryService {

    /**
     * Save a territory.
     *
     * @param territoryDTO the entity to save.
     * @return the persisted entity.
     */
    TerritoryDTO save(TerritoryDTO territoryDTO);

    /**
     * Get all the territories.
     *
     * @return the list of entities.
     */
    List<TerritoryDTO> findAll();


    /**
     * Get the "id" territory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TerritoryDTO> findOne(Long id);

    /**
     * Delete the "id" territory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    public String openWeatherByTerritory(Long id) throws IOException;

    public String saveOpenWeather(Long id) throws IOException, JSONException;

    String getTerritoryFavorite();
}
