package erp.service;

import erp.service.dto.UserRegionDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link erp.domain.UserRegion}.
 */
public interface UserRegionService {

    /**
     * Save a userRegion.
     *
     * @param userRegionDTO the entity to save.
     * @return the persisted entity.
     */
    UserRegionDTO save(UserRegionDTO userRegionDTO);

    /**
     * Get all the userRegions.
     *
     * @return the list of entities.
     */
    List<UserRegionDTO> findAll();


    /**
     * Get the "id" userRegion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserRegionDTO> findOne(Long id);

    /**
     * Delete the "id" userRegion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
