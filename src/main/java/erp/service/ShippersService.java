package erp.service;

import erp.service.dto.ShippersDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link erp.domain.Shippers}.
 */
public interface ShippersService {

    /**
     * Save a shippers.
     *
     * @param shippersDTO the entity to save.
     * @return the persisted entity.
     */
    ShippersDTO save(ShippersDTO shippersDTO);

    /**
     * Get all the shippers.
     *
     * @return the list of entities.
     */
    List<ShippersDTO> findAll();


    /**
     * Get the "id" shippers.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShippersDTO> findOne(Long id);

    /**
     * Delete the "id" shippers.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
