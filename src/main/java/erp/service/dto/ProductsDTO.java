package erp.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;

/**
 * A DTO for the {@link erp.domain.Products} entity.
 */
@ApiModel(description = "Task entity.\n@author The JHipster team.")
public class ProductsDTO implements Serializable {
    
    private Long id;

    private String productName;

    private Long categoryID;

    private Long supplierID;

    private Long unitPrice;


    private Long orderDetailsId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Long categoryID) {
        this.categoryID = categoryID;
    }

    public Long getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(Long supplierID) {
        this.supplierID = supplierID;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Long getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(Long orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductsDTO)) {
            return false;
        }

        return id != null && id.equals(((ProductsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductsDTO{" +
            "id=" + getId() +
            ", productName='" + getProductName() + "'" +
            ", categoryID=" + getCategoryID() +
            ", supplierID=" + getSupplierID() +
            ", unitPrice=" + getUnitPrice() +
            ", orderDetailsId=" + getOrderDetailsId() +
            "}";
    }
}
