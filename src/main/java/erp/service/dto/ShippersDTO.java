package erp.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link erp.domain.Shippers} entity.
 */
public class ShippersDTO implements Serializable {
    
    private Long id;

    private String companyName;

    private String phoneNumber;


    private Long ordersId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(Long ordersId) {
        this.ordersId = ordersId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShippersDTO)) {
            return false;
        }

        return id != null && id.equals(((ShippersDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ShippersDTO{" +
            "id=" + getId() +
            ", companyName='" + getCompanyName() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", ordersId=" + getOrdersId() +
            "}";
    }
}
