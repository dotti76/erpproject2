package erp.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link erp.domain.UserTerritory} entity.
 */
public class UserTerritoryDTO implements Serializable {
    
    private Long id;

    private Long territoryId;

    private Long userId;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTerritoryId() {
        return territoryId;
    }

    public void setTerritoryId(Long territoryId) {
        this.territoryId = territoryId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserTerritoryDTO)) {
            return false;
        }

        return id != null && id.equals(((UserTerritoryDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserTerritoryDTO{" +
            "id=" + getId() +
            ", territoryId=" + getTerritoryId() +
            ", userId=" + getUserId() +
            "}";
    }
}
