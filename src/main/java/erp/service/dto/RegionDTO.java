package erp.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link erp.domain.Region} entity.
 */
public class RegionDTO implements Serializable {
    
    private Long id;

    private String regionName;


    private Long userRegionId;

    private Long territoryId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Long getUserRegionId() {
        return userRegionId;
    }

    public void setUserRegionId(Long userRegionId) {
        this.userRegionId = userRegionId;
    }

    public Long getTerritoryId() {
        return territoryId;
    }

    public void setTerritoryId(Long territoryId) {
        this.territoryId = territoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegionDTO)) {
            return false;
        }

        return id != null && id.equals(((RegionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RegionDTO{" +
            "id=" + getId() +
            ", regionName='" + getRegionName() + "'" +
            ", userRegionId=" + getUserRegionId() +
            ", territoryId=" + getTerritoryId() +
            "}";
    }
}
