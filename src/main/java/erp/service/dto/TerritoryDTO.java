package erp.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;

/**
 * A DTO for the {@link erp.domain.Territory} entity.
 */
@ApiModel(description = "not an ignored comment")
public class TerritoryDTO implements Serializable {
    
    private Long id;

    private Long regionID;

    private String name;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRegionID() {
        return regionID;
    }

    public void setRegionID(Long regionID) {
        this.regionID = regionID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TerritoryDTO)) {
            return false;
        }

        return id != null && id.equals(((TerritoryDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TerritoryDTO{" +
            "id=" + getId() +
            ", regionID=" + getRegionID() +
            ", name='" + getName() + "'" +
            "}";
    }
}
