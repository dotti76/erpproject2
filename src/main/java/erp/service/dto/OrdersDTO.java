package erp.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link erp.domain.Orders} entity.
 */
public class OrdersDTO implements Serializable {

    private Long id;

    private Long customerID;

    private Long employeeID;

    private Instant orderDate;

    private Instant shippedDate;

    private Long shipVia;

    private String shipAddress;

    private String shipCity;

    private String freight;


    private Long orderDetailsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Long employeeID) {
        this.employeeID = employeeID;
    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }

    public Instant getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(Instant shippedDate) {
        this.shippedDate = shippedDate;
    }

    public Long getShipVia() {
        return shipVia;
    }

    public void setShipVia(Long shipVia) {
        this.shipVia = shipVia;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getFreight() {
        return freight;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public Long getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(Long orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrdersDTO)) {
            return false;
        }

        return id != null && id.equals(((OrdersDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdersDTO{" +
            "id=" + getId() +
            ", customerID=" + getCustomerID() +
            ", employeeID=" + getEmployeeID() +
            ", orderDate='" + getOrderDate() + "'" +
            ", shippedDate='" + getShippedDate() + "'" +
            ", shipVia=" + getShipVia() +
            ", shipAddress='" + getShipAddress() + "'" +
            ", shipCity='" + getShipCity() + "'" +
            ", freight='" + getFreight() + "'" +
            ", orderDetailsId=" + getOrderDetailsId() +
            "}";
    }

    public OrdersDTO(Long id, Long customerID, Long employeeID, Instant orderDate, Instant shippedDate, Long shipVia, String shipAddress, String shipCity, String freight, Long orderDetailsId) {
        this.id = id;
        this.customerID = customerID;
        this.employeeID = employeeID;
        this.orderDate = orderDate;
        this.shippedDate = shippedDate;
        this.shipVia = shipVia;
        this.shipAddress = shipAddress;
        this.shipCity = shipCity;
        this.freight = freight;
        this.orderDetailsId = orderDetailsId;
    }

    public OrdersDTO() {
    }
}
