package erp.service.dto;

public class OrdersCompleteDTO extends OrdersDTO{

    private Long productID;

    private Long unitPrice;

    private Long quantity;


    public Long getProductID() {
        return productID;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public OrdersCompleteDTO() {
        super();
    }

    public OrdersCompleteDTO(Long productID, Long unitPrice, Long quantity) {
        super();
        this.productID = productID;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    @java.lang.Override
    public java.lang.String toString() {

        return "OrdersCompleteDTO{" + super.toString() +
            "productID=" + productID +
            ", unitPrice=" + unitPrice +
            ", quantity=" + quantity +
            '}';
    }
}
