package erp.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link erp.domain.Suppliers} entity.
 */
public class SuppliersDTO implements Serializable {
    
    private Long id;

    private String companyName;


    private Long productsId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getProductsId() {
        return productsId;
    }

    public void setProductsId(Long productsId) {
        this.productsId = productsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SuppliersDTO)) {
            return false;
        }

        return id != null && id.equals(((SuppliersDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SuppliersDTO{" +
            "id=" + getId() +
            ", companyName='" + getCompanyName() + "'" +
            ", productsId=" + getProductsId() +
            "}";
    }
}
