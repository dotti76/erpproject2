package erp.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link erp.domain.UserRegion} entity.
 */
public class UserRegionDTO implements Serializable {
    
    private Long id;

    private Long regionID;

    private Long userId;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRegionID() {
        return regionID;
    }

    public void setRegionID(Long regionID) {
        this.regionID = regionID;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserRegionDTO)) {
            return false;
        }

        return id != null && id.equals(((UserRegionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserRegionDTO{" +
            "id=" + getId() +
            ", regionID=" + getRegionID() +
            ", userId=" + getUserId() +
            "}";
    }
}
