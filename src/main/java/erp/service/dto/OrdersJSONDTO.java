package erp.service.dto;

import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link erp.domain.OrdersJSON} entity.
 */
public class OrdersJSONDTO implements Serializable {

    private String id;

    private Long customerID;

    private Long employeeID;

    private Instant orderDate;

    private Instant shippedDate;

    private Long shipVia;

    private String shipAddress;

    private String shipCity;

    private String freight;

    private Long productID;

    private Long unitPrice;

    private Long quantity;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Long employeeID) {
        this.employeeID = employeeID;
    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }

    public Instant getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(Instant shippedDate) {
        this.shippedDate = shippedDate;
    }

    public Long getShipVia() {
        return shipVia;
    }

    public void setShipVia(Long shipVia) {
        this.shipVia = shipVia;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getFreight() {
        return freight;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public Long getProductID() {
        return productID;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrdersJSONDTO)) {
            return false;
        }

        return id != null && id.equals(((OrdersJSONDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdersJSONDTO{" +
            "id=" + getId() +
            ", customerID=" + getCustomerID() +
            ", employeeID=" + getEmployeeID() +
            ", orderDate='" + getOrderDate() + "'" +
            ", shippedDate='" + getShippedDate() + "'" +
            ", shipVia=" + getShipVia() +
            ", shipAddress='" + getShipAddress() + "'" +
            ", shipCity='" + getShipCity() + "'" +
            ", freight='" + getFreight() + "'" +
            ", productID=" + getProductID() +
            ", unitPrice=" + getUnitPrice() +
            ", quantity=" + getQuantity() +
            "}";
    }

    public OrdersJSONDTO() {
    }

    public OrdersJSONDTO(String id, Long customerID, Long employeeID, Instant orderDate, Instant shippedDate, Long shipVia, String shipAddress, String shipCity, String freight, Long productID, Long unitPrice, Long quantity) {
        this.id = id;
        this.customerID = customerID;
        this.employeeID = employeeID;
        this.orderDate = orderDate;
        this.shippedDate = shippedDate;
        this.shipVia = shipVia;
        this.shipAddress = shipAddress;
        this.shipCity = shipCity;
        this.freight = freight;
        this.productID = productID;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }
}
