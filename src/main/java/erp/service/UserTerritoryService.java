package erp.service;

import erp.service.dto.UserTerritoryDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link erp.domain.UserTerritory}.
 */
public interface UserTerritoryService {

    /**
     * Save a userTerritory.
     *
     * @param userTerritoryDTO the entity to save.
     * @return the persisted entity.
     */
    UserTerritoryDTO save(UserTerritoryDTO userTerritoryDTO);

    /**
     * Get all the userTerritories.
     *
     * @return the list of entities.
     */
    List<UserTerritoryDTO> findAll();


    /**
     * Get the "id" userTerritory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserTerritoryDTO> findOne(Long id);

    /**
     * Delete the "id" userTerritory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
