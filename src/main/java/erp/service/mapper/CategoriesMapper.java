package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.CategoriesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Categories} and its DTO {@link CategoriesDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductsMapper.class})
public interface CategoriesMapper extends EntityMapper<CategoriesDTO, Categories> {

    @Mapping(source = "products.id", target = "productsId")
    CategoriesDTO toDto(Categories categories);

    @Mapping(source = "productsId", target = "products")
    Categories toEntity(CategoriesDTO categoriesDTO);

    default Categories fromId(Long id) {
        if (id == null) {
            return null;
        }
        Categories categories = new Categories();
        categories.setId(id);
        return categories;
    }
}
