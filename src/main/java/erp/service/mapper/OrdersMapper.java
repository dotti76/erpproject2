package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.OrdersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Orders} and its DTO {@link OrdersDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrderDetailsMapper.class})
public interface OrdersMapper extends EntityMapper<OrdersDTO, Orders> {

    @Mapping(source = "orderDetails.id", target = "orderDetailsId")
    OrdersDTO toDto(Orders orders);

    @Mapping(source = "orderDetailsId", target = "orderDetails")
    @Mapping(target = "customerIDS", ignore = true)
    @Mapping(target = "removeCustomerID", ignore = true)
    @Mapping(target = "shipVias", ignore = true)
    @Mapping(target = "removeShipVia", ignore = true)
    Orders toEntity(OrdersDTO ordersDTO);

    default Orders fromId(Long id) {
        if (id == null) {
            return null;
        }
        Orders orders = new Orders();
        orders.setId(id);
        return orders;
    }
}
