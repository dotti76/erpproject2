package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.OrderDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrderDetails} and its DTO {@link OrderDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrderDetailsMapper extends EntityMapper<OrderDetailsDTO, OrderDetails> {


    @Mapping(target = "orderIDS", ignore = true)
    @Mapping(target = "removeOrderID", ignore = true)
    @Mapping(target = "productIDS", ignore = true)
    @Mapping(target = "removeProductID", ignore = true)
    OrderDetails toEntity(OrderDetailsDTO orderDetailsDTO);

    default OrderDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setId(id);
        return orderDetails;
    }
}
