package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.RegionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Region} and its DTO {@link RegionDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserRegionMapper.class, TerritoryMapper.class})
public interface RegionMapper extends EntityMapper<RegionDTO, Region> {

    @Mapping(source = "userRegion.id", target = "userRegionId")
    @Mapping(source = "territory.id", target = "territoryId")
    RegionDTO toDto(Region region);

    @Mapping(source = "userRegionId", target = "userRegion")
    @Mapping(source = "territoryId", target = "territory")
    Region toEntity(RegionDTO regionDTO);

    default Region fromId(Long id) {
        if (id == null) {
            return null;
        }
        Region region = new Region();
        region.setId(id);
        return region;
    }
}
