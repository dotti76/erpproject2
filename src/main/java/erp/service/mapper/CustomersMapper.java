package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.CustomersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Customers} and its DTO {@link CustomersDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrdersMapper.class})
public interface CustomersMapper extends EntityMapper<CustomersDTO, Customers> {

    @Mapping(source = "orders.id", target = "ordersId")
    CustomersDTO toDto(Customers customers);

    @Mapping(source = "ordersId", target = "orders")
    Customers toEntity(CustomersDTO customersDTO);

    default Customers fromId(Long id) {
        if (id == null) {
            return null;
        }
        Customers customers = new Customers();
        customers.setId(id);
        return customers;
    }
}
