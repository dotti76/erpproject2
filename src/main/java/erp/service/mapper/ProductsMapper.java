package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.ProductsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Products} and its DTO {@link ProductsDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrderDetailsMapper.class})
public interface ProductsMapper extends EntityMapper<ProductsDTO, Products> {

    @Mapping(source = "orderDetails.id", target = "orderDetailsId")
    ProductsDTO toDto(Products products);

    @Mapping(source = "orderDetailsId", target = "orderDetails")
    @Mapping(target = "categoryIDS", ignore = true)
    @Mapping(target = "removeCategoryID", ignore = true)
    @Mapping(target = "supplierIDS", ignore = true)
    @Mapping(target = "removeSupplierID", ignore = true)
    Products toEntity(ProductsDTO productsDTO);

    default Products fromId(Long id) {
        if (id == null) {
            return null;
        }
        Products products = new Products();
        products.setId(id);
        return products;
    }
}
