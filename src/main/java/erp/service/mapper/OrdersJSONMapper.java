package erp.service.mapper;


import erp.domain.OrdersJSON;
import erp.service.dto.OrdersJSONDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link OrdersJSON} and its DTO {@link OrdersJSONDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrdersJSONMapper extends EntityMapper<OrdersJSONDTO, OrdersJSON> {

    default OrdersJSON fromId(String id) {
        if (id == null) {
            return null;
        }
        OrdersJSON ordersJSON = new OrdersJSON();
        ordersJSON.setId(id);
        return ordersJSON;
    }
}
