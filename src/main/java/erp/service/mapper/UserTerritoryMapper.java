package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.UserTerritoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserTerritory} and its DTO {@link UserTerritoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserTerritoryMapper extends EntityMapper<UserTerritoryDTO, UserTerritory> {



    default UserTerritory fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserTerritory userTerritory = new UserTerritory();
        userTerritory.setId(id);
        return userTerritory;
    }
}
