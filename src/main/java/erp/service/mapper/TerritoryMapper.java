package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.TerritoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Territory} and its DTO {@link TerritoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TerritoryMapper extends EntityMapper<TerritoryDTO, Territory> {


    @Mapping(target = "regionIDS", ignore = true)
    @Mapping(target = "removeRegionID", ignore = true)
    Territory toEntity(TerritoryDTO territoryDTO);

    default Territory fromId(Long id) {
        if (id == null) {
            return null;
        }
        Territory territory = new Territory();
        territory.setId(id);
        return territory;
    }
}
