package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.ShippersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Shippers} and its DTO {@link ShippersDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrdersMapper.class})
public interface ShippersMapper extends EntityMapper<ShippersDTO, Shippers> {

    @Mapping(source = "orders.id", target = "ordersId")
    ShippersDTO toDto(Shippers shippers);

    @Mapping(source = "ordersId", target = "orders")
    Shippers toEntity(ShippersDTO shippersDTO);

    default Shippers fromId(Long id) {
        if (id == null) {
            return null;
        }
        Shippers shippers = new Shippers();
        shippers.setId(id);
        return shippers;
    }
}
