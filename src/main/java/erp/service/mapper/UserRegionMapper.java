package erp.service.mapper;


import erp.domain.*;
import erp.service.dto.UserRegionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserRegion} and its DTO {@link UserRegionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserRegionMapper extends EntityMapper<UserRegionDTO, UserRegion> {


    @Mapping(target = "regionIDS", ignore = true)
    @Mapping(target = "removeRegionID", ignore = true)
    UserRegion toEntity(UserRegionDTO userRegionDTO);

    default UserRegion fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserRegion userRegion = new UserRegion();
        userRegion.setId(id);
        return userRegion;
    }
}
