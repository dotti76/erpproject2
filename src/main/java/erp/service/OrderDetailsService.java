package erp.service;

import erp.service.dto.OrderDetailsDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link erp.domain.OrderDetails}.
 */
public interface OrderDetailsService {

    /**
     * Save a orderDetails.
     *
     * @param orderDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    OrderDetailsDTO save(OrderDetailsDTO orderDetailsDTO);

    /**
     * Get all the orderDetails.
     *
     * @return the list of entities.
     */
    List<OrderDetailsDTO> findAll();


    /**
     * Get the "id" orderDetails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrderDetailsDTO> findOne(Long id);

    /**
     * Delete the "id" orderDetails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
