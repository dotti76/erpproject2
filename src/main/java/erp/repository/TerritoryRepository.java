package erp.repository;

import erp.domain.Territory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Territory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TerritoryRepository extends JpaRepository<Territory, Long> {
}
