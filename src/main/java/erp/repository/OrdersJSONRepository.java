package erp.repository;

import erp.domain.OrdersJSON;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the OrdersJSON entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrdersJSONRepository extends MongoRepository<OrdersJSON, String> {
}
