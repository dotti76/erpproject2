package erp.repository;

import erp.domain.Shippers;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Shippers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShippersRepository extends JpaRepository<Shippers, Long> {
}
