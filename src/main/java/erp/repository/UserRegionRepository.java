package erp.repository;

import erp.domain.UserRegion;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the UserRegion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserRegionRepository extends JpaRepository<UserRegion, Long> {
}
