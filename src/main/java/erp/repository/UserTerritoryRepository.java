package erp.repository;

import erp.domain.UserTerritory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the UserTerritory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserTerritoryRepository extends JpaRepository<UserTerritory, Long> {
}
