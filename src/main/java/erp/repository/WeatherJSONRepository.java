package erp.repository;

import erp.domain.WeatherJSON;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface WeatherJSONRepository extends MongoRepository<WeatherJSON, String> {
}
